#ifndef LIB_BASECOM_H
#define LIB_BASECOM_H

#include <ros/ros.h>
#include <iostream>
#include <cmath>
#include <chrono>
#include <ctime>

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <std_msgs/String.h>
#include <geometry_msgs/Point32.h>

using std::cout;
using std::endl;
using std::list;
using std::string;
using std::to_string;
using std::vector;

const float ROS_RATE_HZ = 0.5;

typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;

class Battery
{
public:
  Battery();
  ~Battery();

  void Execute();
  void Mission();
  bool Initial();

private:
  /** Node Handles **/
  ros::NodeHandle n;
  ros::NodeHandle pn;

  /** Topics **/
  ros::Publisher battery_pub;
  ros::Subscriber cmd_sub;

  /** Variables **/
  string port_name_;
  int baud_rate_;

  serial_port_ptr port_;
  boost::system::error_code error_code_;
  boost::asio::io_service io_service_;

  ros::Time current_time_;
  ros::Time read_time_;

  ros::Time timer_cmd_;
  double cmd_timeout_sec_;
  double read_timeout_sec_;

    // 正常工作电压范围
  const double minVoltage = 46.0;
  const double maxVoltage = 54.6;

  // 电量百分比范围
  const int minPercentage = 0;
  const int maxPercentage = 100;

  geometry_msgs::Point32 m_vol;

  list<float> battery_history_;
  bool isConnected_;

  /** Functions **/
  bool openPort();
  bool closePort();
  bool reconnectPort();

  void sendBaseData(unsigned char dataId);
  vector<unsigned char> readBaseData();

  void sendSOCCmd();
  void sendChargeCmd();
  void readSOC();
  void readCharge();

  unsigned char calculateChecksum(const std::vector<unsigned char> &data);
};

#endif
