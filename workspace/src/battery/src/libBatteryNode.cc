#include "libBatteryNode.h"

Battery::Battery() : pn("~")
{
  pn.param<string>("port_name", port_name_, "/dev/battery");
  pn.param<int>("baud_rate", baud_rate_, 9600);

  battery_pub = n.advertise<geometry_msgs::Point32>("/battery_info", 1);

  cout << "battery Node Started" << endl;

  Initial();
}

Battery::~Battery()
{
  closePort();
}

bool Battery::Initial()
{
  cmd_timeout_sec_ = 0.1;
  read_timeout_sec_ = 1;

  cout << "Base Node Initialed" << endl;
  isConnected_ = false;
  return true;
}

void Battery::Execute()
{
  ros::Rate loop_rate(ROS_RATE_HZ);

  openPort();

  while (ros::ok())
  {
    Mission();
    loop_rate.sleep();
    ros::spinOnce();
  }

  closePort();
}

void Battery::Mission()
{
  if (!isConnected_)
  {
    reconnectPort();
    return;
  };
  sendSOCCmd();
  sleep(2); // 等待下位机处理指令
  readSOC();

  sendChargeCmd();
  sleep(2);
  readCharge();

  battery_pub.publish(m_vol);
}

bool Battery::openPort()
{
  cout << "Opening Port" << endl;
  if (isConnected_)
  {
    cout << "Port Has Opened" << endl;
    return true;
  }
  port_ = serial_port_ptr(new boost::asio::serial_port(io_service_));
  port_->open(port_name_, error_code_);
  if (error_code_)
  {
    ROS_INFO_STREAM("error : port_->open() failed...port_name=" << port_name_ << ", e=" << error_code_.message().c_str());
    closePort();
    isConnected_ = false;
    return false;
  }
  port_->set_option(boost::asio::serial_port_base::baud_rate(baud_rate_));
  cout << "Port Opened" << endl;
  isConnected_ = true;
  return true;
}

bool Battery::closePort()
{
  if (!isConnected_)
  {
    cout << "Port Has Closed" << endl;
    return true;
  }
  if (port_)
  {
    port_->cancel();
    port_->close();
    port_.reset();
  }
  io_service_.stop();
  io_service_.reset();
  cout << "Port Closed" << endl;
  isConnected_ = false;
  return true;
}

bool Battery::reconnectPort()
{
  ROS_INFO_STREAM("Reconnecting Battery.");
  port_.reset();
  io_service_.stop();
  io_service_.reset();
  port_ = serial_port_ptr(new boost::asio::serial_port(io_service_)); // port_ 指向新创建的端口(开启IO)的地址
  port_->open(port_name_, error_code_);                               // 打开端口，失败则返回一个error code
  if (error_code_)
  {
    ROS_ERROR_STREAM("Unable to Reconnect " << port_name_);
    isConnected_ = false;
    return false;
  }
  else
  {
    port_->set_option(boost::asio::serial_port_base::baud_rate(baud_rate_));
    isConnected_ = true;
    ROS_INFO_STREAM(port_name_ << " Reconnected");
    return true;
  }
}

void Battery::sendBaseData(unsigned char _dataId)
{

  // |帧头|地址|数据ID|数据长度|数据内容|校验和
  vector<unsigned char> output = {0xA5}; // 帧头
  unsigned char address = 0x40;          // 地址
  unsigned char dataId = _dataId;        // 数据ID
  unsigned char dataLen = 0x08;          // 数据长度
  vector<unsigned char> dataContent(8, 0);

  output.push_back(address);
  output.push_back(dataId);
  output.push_back(dataLen);
  output.insert(output.end(), dataContent.begin(), dataContent.end());

  unsigned char checksum = calculateChecksum(output);
  output.push_back(checksum);

  // a5 40 93 08 00 00 00 00 00 00 00 00 80 

  // cout << endl;
  // cout << "Command Raw :";
  // for (int i = 0; i < output.size(); ++i)
  // {
  //   printf(" %d:%02x ", i, output[i]);
  // }
  // cout << endl;

  boost::asio::write(*port_.get(), boost::asio::buffer(output), boost::asio::transfer_all(), error_code_);
}

void Battery::sendSOCCmd()
{
  unsigned char dataId = 0x90; // 数据ID
  sendBaseData(dataId);
}

void Battery::sendChargeCmd()
{
  unsigned char dataId = 0x93; // 数据ID
  sendBaseData(dataId);
}

vector<unsigned char> Battery::readBaseData()
{
  int char_size = 13; //
  vector<unsigned char> output(char_size);
  size_t len = port_->read_some(boost::asio::buffer(output), error_code_);

  if (error_code_)
  {
    sleep(0.5);
    reconnectPort();
  }

  if (len <= 0)
    return vector<unsigned char>();

  unsigned char check = output.back();
  output.pop_back();
  if (calculateChecksum(output) == check)
  {
    return output;
  }
  else
  {
    return vector<unsigned char>();
  }
}

void Battery::readSOC()
{
  vector<unsigned char> output = readBaseData();
  if (output.empty())
  {
    ROS_ERROR("readSOC err");
  }
  else
  {
    // std::stringstream ss;
    // ss << "Base upload raw :";
    // for (int i = 0; i < 12; ++i)
    // {
    //   ss << " " << i << ":" << std::setw(2) << std::setfill('0') << std::hex << static_cast<int>(output[i]);
    // }
    // ROS_INFO_STREAM(ss.str());

    int voltage_upload = (output[4] << 8) | output[5];
    double voltage = static_cast<double>(voltage_upload) / 10; // 电压
    m_vol.y = voltage;

    double percentage;
    if (voltage < minVoltage)
    {
      percentage = minPercentage;
      return;
    }

    // 如果电压高于最大工作电压，返回100%
    if (voltage > maxVoltage)
    {
      percentage = maxPercentage;
      return;
    }

    // 线性映射到剩余电量百分比范围
    percentage = minPercentage + (voltage - minVoltage) / (maxVoltage - minVoltage) * (maxPercentage - minPercentage);

    m_vol.z = percentage;
    std::cout << "percentage：" << m_vol.z << std::endl;
  }
}

void Battery::readCharge()
{
  vector<unsigned char> output = readBaseData();
  if (output.empty())
  {
    ROS_ERROR("readCharge err");
  }
  else
  {
    int chargeState = output[4];          // （0 静止，1 充电，2 放电）
    m_vol.x = (chargeState == 1 ? 1 : 0); // （0 放电 1 充电 ）
    // std::cout << "charge" << m_vol.x << std::endl;
  }
}

// 计算校验和
unsigned char Battery::calculateChecksum(const std::vector<unsigned char> &data)
{
  unsigned char checksum = 0;
  for (const auto &byte : data)
  {
    checksum += byte;
  }
  return checksum & 0xFF; // 只取低字节
}
