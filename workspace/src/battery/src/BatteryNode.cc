#include "libBatteryNode.h"

int main(int argc, char ** argv) {
  ros::init(argc, argv, "battery_node");
  Battery MyControl;
  MyControl.Execute();
  return 0;
}
