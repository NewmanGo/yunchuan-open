//
// Copyright (c) 2021 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#include "drivers/ethernet.h"

#include <fcntl.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#ifdef _WIN32
#include <WinSock2.h>
#include <windows.h>
#include <chrono>
#elif __linux__
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <arpa/inet.h>
#else
#error This platform does not support!
#endif

#include <iostream>
#include <thread>

Ethernet::Ethernet()
    : ip_(), port_(0), fd_(-1), handler_(nullptr), quit_(false), try_to_quit_(false) {}

Ethernet::~Ethernet()
{
    try_to_quit_ = true;
    while (!quit_);

    if (fd_ < 0)
        return;

#ifdef _WIN32
    closesocket(fd_);
    WSACleanup();
#elif __linux__
    close(fd_);
#endif
}

std::unique_ptr<Ethernet> Ethernet::Create(uint16_t listen_port, const std::string &ip, uint16_t port)
{
    std::unique_ptr<Ethernet> ethernet(new Ethernet());
    if (ethernet == nullptr)
        return nullptr;

    if (!ethernet->Init(listen_port))
        return nullptr;

    return ethernet;
}

bool Ethernet::Launch(Handler handler, bool detached, int sync_interval, int sync_timeout)
{
    this->handler_ = handler;
    if (fd_ < 0) {
        std::cerr << "Failed to find available socket" << std::endl;
        return false;
    }

    // [1] create epoll object
    int fd = epoll_create(1);

    // [2] add listen object
    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = fd_;

    if (epoll_ctl(fd, EPOLL_CTL_ADD, fd_, &event) < 0) {
        std::cerr << "Failed to add monitored object" << std::endl;
        return false;
    }

    std::thread t(&Ethernet::Handle, this, fd);

    if (detached) {
        t.detach();
    } else {
        t.join();
    }

    return true;
}

bool Ethernet::Init(uint16_t listen_port)
{
#ifdef _WIN32
    WSADATA wsadata;
    if (WSAStartup(MAKEWORD(2, 2), &wsadata) != 0) {
        std::cerr << "Failed to initialize socket dll" << std::endl;
        return false;
    }
#endif

    fd_ = ::socket(AF_INET, SOCK_DGRAM, 0);
    if (fd_ < 0) {
        std::cerr << "Failed to create socket." << std::endl;
#ifdef _WIN32
        WSACleanup();
#endif
        return false;
    }

    struct sockaddr_in local;
    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(listen_port);

    if (bind(fd_, (struct sockaddr *)&local, sizeof(local)) < 0) {
        std::cerr << "Failed to bind port." << std::endl;
        return false;
    }

    return true;
}

bool Ethernet::Write(const std::string &cmd)
{
    if (fd_ < 0) {
        std::cerr << "Failed to find available socket" << std::endl;
        return false;
    }

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(ip_.c_str());
    server.sin_port = htons(port_);
    uint32_t length = sizeof(server);

    int res = sendto(fd_, cmd.c_str(), cmd.length(), 0, (struct sockaddr *)&server, length);
    if (res < 0)
        return false;

    return true;
}

void Ethernet::Handle(int fd)
{
    struct epoll_event events[1];

    while (true) {
        // [3] wait event
        int n = epoll_wait(fd, events, 1, -1);
        if (n < 0) {
            if (try_to_quit_)
                break;

            std::cerr << "Failed to wait response" << std::endl;
            continue;
        }

        assert(n == 1);
        if (events[0].events & EPOLLIN) {
            auto Read = [&](uint8_t *data, int length) -> int {
                return ::read(events[0].data.fd, data, length);
            };

            this->handler_(Read);
        }

        if (try_to_quit_)
            break;
    }

    quit_ = true;
}