//
// Copyright (c) 2021 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#ifdef __linux__

#include "drivers/ethernet.h"

#include <fcntl.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include <iostream>
#include <thread>

#include <sys/time.h>

#include "drivers/message.h"
#include "drivers/lidar.h"

static uint64_t get_time(void)
{
    struct timeval tv = {0};
    gettimeofday(&tv, NULL);

    uint64_t time = tv.tv_sec * 1e9 + tv.tv_usec * 1e3;
    return time;
}

int Ethernet::fd_ = -1;

std::mutex Ethernet::mtx_;
std::map<std::string, LidarInfo> Ethernet::handlers_; 

Ethernet::Ethernet()
    : ip_(), port_(0), handler_(nullptr), quit_(false), try_to_quit_(false) {}

Ethernet::Ethernet(std::string ip)
    : ip_(ip), port_(0), handler_(nullptr), quit_(false), try_to_quit_(false) {}

Ethernet::~Ethernet()
{
    try_to_quit_ = true;
    while (!quit_);

    if (fd_ < 0)
        return;

    close(fd_);

    fd_ = -1;
}

std::unique_ptr<Ethernet> Ethernet::Create(uint16_t listen_port, const std::string &ip, uint16_t port)
{
    std::unique_ptr<Ethernet> ethernet(new Ethernet(ip));
    if (ethernet == nullptr)
        return nullptr;
    /*
    if (!ethernet->Init(listen_port))
        return nullptr;
    */

    ethernet->port_ = port;

    int init_cnt = 0;
    /*
    ::mtx_.lock();
    init_cnt = handlers_.size();
    ::mtx_.unlock();
    */

    mtx_.lock();
    init_cnt = handlers_.size();
    mtx_.unlock();

    if(init_cnt > 0)
    {
        return ethernet;
    }

    if (!ethernet->Init(listen_port))
    {
        std::cerr << "socket init err" << std::endl;
    }

    return ethernet;
}




bool Ethernet::Launch(Handler handler, bool detached, int sync_interval, int sync_timeout)
{
    this->handler_ = handler;

    LidarInfo li;
    li.pre_recv_time = 0;
    li.cur_recv_time = 0;
    li.sync_end_time = 0;
    li.send_cmd_time = 0;
    li.sync_state = 0;
    li.sync_retry = 0;
    li.sync_interval = sync_interval;
    li.sync_timeout = sync_timeout;
    li.hfunc = handler;
    li.crhfunc = ctl_run_handler_;
    li.last_pub_time = get_time();
    li.last_log_time = li.last_pub_time;

    //std::cout << "ip: " << ip_ << " " <<  sync_interval << " " << sync_timeout << std::endl;

    int init_cnt = 0;

    mtx_.lock();
    handlers_[ip_] = li;
    init_cnt = handlers_.size();
    mtx_.unlock();

    if(init_cnt > 1)
    {
        return true;
    }
    
    if (fd_ < 0) {
        std::cerr << "Failed to find available socket" << std::endl;
        return false;
    }

    // [1] create epoll object
    int fd = epoll_create(1);

    // [2] add listen object
    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = fd_;

    if (epoll_ctl(fd, EPOLL_CTL_ADD, fd_, &event) < 0) {
        std::cerr << "Failed to add monitored object" << std::endl;
        return false;
    }

    std::thread t(&Ethernet::Handle, this, fd);

    if (detached) {
        t.detach();
    } else {
        t.join();
    }

    return true;
}

bool Ethernet::Init(uint16_t listen_port)
{
    fd_ = ::socket(AF_INET, SOCK_DGRAM, 0);
    if (fd_ < 0) {
        std::cerr << "Failed to create socket." << std::endl;
        return false;
    }

    //int nRecvBuf = 262144;// max:212992*2
    //setsockopt(fd_, SOL_SOCKET, SO_RCVBUF, &nRecvBuf, sizeof(int));

    int nRecvBuf = 0;
    int len = sizeof(int);
    if(0 == getsockopt(fd_, SOL_SOCKET, SO_RCVBUF, &nRecvBuf, (socklen_t*)&len))
    {
        //std::cout << "recvbuf size: " << nRecvBuf << std::endl;
        nRecvBuf = nRecvBuf * 2;
        setsockopt(fd_, SOL_SOCKET, SO_RCVBUF, &nRecvBuf, sizeof(int));
        //getsockopt(fd_, SOL_SOCKET, SO_RCVBUF, &nRecvBuf, (socklen_t*)&len);
        //std::cout << "recvbuf size: " << nRecvBuf << std::endl;
    }

    struct sockaddr_in local;
    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(listen_port);

    if (bind(fd_, (struct sockaddr *)&local, sizeof(local)) < 0) {
        std::cerr << "Failed to bind port." << std::endl;
        close(fd_);//add by fang
        fd_ = -1;
        return false;
    }

    return true;
}

bool Ethernet::Write(const std::string &cmd)
{
    if (fd_ < 0) {
        std::cerr << "Failed to find available socket" << std::endl;
        return false;
    }

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(ip_.c_str());
    server.sin_port = htons(port_);
    uint32_t length = sizeof(server);

    int res = sendto(fd_, cmd.c_str(), cmd.length(), 0, (struct sockaddr *)&server, length);
    if (res < 0)
        return false;

    return true;
}

bool Ethernet::Write(unsigned char* cmd, size_t len)
{
    if (fd_ < 0) {
        std::cerr << "Failed to find available socket" << std::endl;
        return false;
    }

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(ip_.c_str());
    server.sin_port = htons(port_);
    uint32_t length = sizeof(server);

    int res = sendto(fd_, cmd, len, 0, (struct sockaddr *)&server, length);
    if (res < 0)
        return false;

    return true;
}

bool Ethernet::Write(std::string ip, char* cmd, size_t len)
{
    if (fd_ < 0) {
        std::cerr << "Failed to find available socket" << std::endl;
        return false;
    }

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(ip.c_str());
    server.sin_port = htons(port_);
    uint32_t length = sizeof(server);

    int res = sendto(fd_, cmd, len, 0, (struct sockaddr *)&server, length);
    if (res < 0)
        return false;

    return true;
}

bool Ethernet::GetCtlRun()
{
    return ctl_run_handler_();
}

bool Ethernet::SetCtlRunHandler(CtlRunHandler handler)
{
    ctl_run_handler_ = handler;

    return true;
}

void Ethernet::Handle(int fd)
{
    struct epoll_event events[1];
    uint64_t dif_time = 0;
    int sync_flag = -1;

    while (true) {
        // [3] wait event
        int n = epoll_wait(fd, events, 1, 0.7*1000);
        if (n < 0) {
            if (try_to_quit_)
                break;

            std::cerr << "Failed to wait response" << std::endl;
            continue;
        }

        //assert(n == 1);
        if ( (n > 0) && (events[0].events & EPOLLIN) ) {
            /*
            auto Read = [&](uint8_t *data, int length) -> int {
                return ::read(events[0].data.fd, data, length);
            };

            this->handler_(Read);
            */
            struct sockaddr_in read_addr;
            socklen_t read_addr_length = sizeof(read_addr);
            unsigned char udp_buf[1440] = {0};
            int udp_len;
            
            
            if( ( (udp_len = recvfrom(events[0].data.fd, udp_buf, 1440, 0, 
                                        (struct sockaddr*)&read_addr, &read_addr_length) ) > 0 )
                                        && (udp_len > 0) )
            {
                char read_ip[64] = {0};
                strncpy(read_ip, inet_ntoa(read_addr.sin_addr), 64);
                //std::cout << read_ip << std::endl;
                
                auto Read = [&](uint8_t *data, int length) -> int {
                    udp_len = (udp_len < length) ? udp_len : length;
                    memcpy(data, udp_buf, udp_len);
                    return udp_len;
                };

                mtx_.lock();
                
                std::map<std::string, LidarInfo>::iterator iter = handlers_.find(std::string(read_ip));
                if(iter != handlers_.end())
                {
                    //printf("ip:%s, buf0:%02X, udp_len:%d\n", read_ip, udp_buf[0], udp_len);
                    iter->second.cur_recv_time = get_time();

                    if(udp_len > 11)
                    {
                        if(iter->second.hfunc(Read))
                        {
                            iter->second.last_pub_time = get_time();
                        }
                    }
                    else if(0xFB == udp_buf[0])
                    {
                        switch(iter->second.sync_state)
                        {
                            case 1:
                            {
                                Message<0> response;
                                if(response.length() == udp_len)
                                {
                                    memcpy(response.bytes(), udp_buf, response.length());
                                    if (OPcode::kPrepare == response.opcode())
                                    {
                                        //printf("ip: %s, recv1\n", read_ip);

                                        iter->second.sync_state = 2;
                                        //send sync
                                        Message<0> sync;
                                        sync.Set(OPcode::kSync, Flags::kNone, nullptr, 0);
                                        Write(iter->first, sync.bytes(), sync.length());

                                        iter->second.send_cmd_time = get_time();
                                        iter->second.sync_state = 3;

                                        //std::cout << "ip: " << read_ip << "sync time: " << iter->second.send_cmd_time << std::endl;

                                        //send follow up
                                        Message<sizeof(iter->second.send_cmd_time)> followup;
                                        followup.Set(OPcode::kFollowUp, Flags::kNone, 
                                            reinterpret_cast<const char *>(&iter->second.send_cmd_time), sizeof(iter->second.send_cmd_time));
                                        Write(iter->first, followup.bytes(), followup.length());
                                        
                                        iter->second.send_cmd_time = get_time();
                                        iter->second.sync_state = 4;
                                    }
                                }
                            }
                            break;

                            case 4:
                            {
                                Message<0> request;
                                if(request.length() == udp_len)
                                {
                                    memcpy(request.bytes(), udp_buf, request.length());
                                    if (OPcode::kDelayRequest == request.opcode())
                                    {
                                        //printf("ip: %s, recv4\n", read_ip);

                                        iter->second.sync_state = 5;

                                        Message<sizeof(iter->second.cur_recv_time)> response;
                                        response.Set(OPcode::kDelayResponse, Flags::kNone, 
                                            reinterpret_cast<const char *>(&iter->second.cur_recv_time), sizeof(iter->second.cur_recv_time));
                                        Write(iter->first, response.bytes(), response.length());

                                        iter->second.send_cmd_time = get_time();
                                        iter->second.sync_state = 6;
                                    }
                                }
                            }
                            break;

                            case 6:
                            {
                                Message<sizeof(iter->second.cur_recv_time)> result;
                                if(result.length() == udp_len)
                                {
                                    memcpy(result.bytes(), udp_buf, result.length());    
                                    if (OPcode::kDelayResponse == result.opcode())
                                    {
                                        //printf("ip: %s, recv6\n", read_ip);

                                        std::cout << "ip: " << read_ip << " delay res: " << *((uint64_t *)result.body()) << std::endl;

                                        iter->second.sync_state = 7;

                                        Message<0> request;
                                        request.Set(OPcode::kEnd, Flags::kNone, nullptr, 0);
                                        Write(iter->first, request.bytes(), request.length());

                                        iter->second.send_cmd_time = get_time();
                                        iter->second.sync_state = 8;
                                    }
                                }
                            }
                            break;

                            case 8:
                            {
                                Message<0> response;
                                if(response.length() == udp_len)
                                {
                                    memcpy(response.bytes(), udp_buf, response.length());
                                    if (OPcode::kEnd == response.opcode())
                                    {
                                        //printf("ip: %s, recv8\n", read_ip);
                                        std::cout << "ip: " << read_ip << " sync time ok" << std::endl;
                                        
                                        iter->second.sync_end_time = get_time();

                                        iter->second.sync_state = 9;
                                        iter->second.sync_retry = 0;
                                    }
                                }
                            }
                            break;

                        }//end of switch

                    }//end of else if(0xFB == udp_buf[0])
                }//end of if(iter != handlers_.end())
                else
                {
                    std::cout << "get data cnt " << udp_len << " from ip: " << read_ip << std::endl;
                }

                mtx_.unlock();
                
            }//end of if( ( (udp_len =
            
        }//end of if ( (n > 0) &&


        //chk do time sync for all
        mtx_.lock();

        std::map<std::string, LidarInfo>::iterator iter;
        for (iter = handlers_.begin(); iter != handlers_.end(); iter++)
        {

            dif_time = get_time() - iter->second.cur_recv_time;
            if(dif_time > (iter->second.sync_timeout)*1e6)
            { 
                iter->second.sync_end_time = 0;
            }
            
            dif_time = get_time() - iter->second.sync_end_time;
            if(dif_time > (iter->second.sync_interval)*1e9)
            {
                dif_time = get_time() - iter->second.send_cmd_time;

                if(dif_time > 0.5*1e9)
                {
                    //std::cout << "do time sync " << "ip: " << iter->first << " " << iter->second.crhfunc() << std::endl;

                    Message<0> request;
                    request.Set(OPcode::kPrepare, Flags::kNone, nullptr, 0);
                    Write(iter->first, request.bytes(), request.length());
                    
                    iter->second.sync_end_time = 0;
                    iter->second.send_cmd_time = get_time();
                    iter->second.sync_state = 1;
                    iter->second.sync_retry = 0;                  
                }
            }

            dif_time = get_time() - iter->second.last_pub_time;
            if( (iter->second.crhfunc()) && (dif_time > 0.8*1e9) )
            {
                //std::cout << "send startlds$" << "ip: " << iter->first << std::endl;

                Write(iter->first, "startlds$", 9);
            }

            dif_time = iter->second.last_pub_time - iter->second.last_log_time;

            if(dif_time > 30*1e9)
            {
                iter->second.last_log_time = iter->second.last_pub_time;
                std::cout << "net publish flag time: " << iter->second.last_pub_time << " ,ip: " << iter->first << std::endl;
                PubLog::log->info("net publish flag time: {} ,ip: {}", iter->second.last_pub_time, iter->first);
            }

        }

        mtx_.unlock();



        if (try_to_quit_)
            break;
    }

    quit_ = true;
}

#endif
