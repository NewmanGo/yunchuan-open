//
// Copyright (c) 2022 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#include "sdkcore.h"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>

#include "common/configurator.h"
#include "common/scripts.h"
#include "drivers/uart.h"
#include "drivers/ethernet.h"
#include "drivers/lidar.h"

namespace {

void ScanCopy(const LScan &scan, LScan &new_scan)
{
    new_scan.stamp = scan.stamp;
    new_scan.interval = scan.interval;

    new_scan.angle_increment = scan.angle_increment;

    new_scan.min_angle = scan.min_angle;
    new_scan.max_angle = scan.max_angle;

    new_scan.min_range = scan.min_range;
    new_scan.max_range = scan.max_range;

    new_scan.ranges.resize(scan.ranges.size());
    new_scan.intensities.resize(scan.intensities.size());
    for (size_t i = 0; i < scan.ranges.size(); i++) {
        new_scan.ranges[i] = scan.ranges[i];
        new_scan.intensities[i] = scan.intensities[i];
    }
}

void SwitchToCartesian(LScan &scan, std::vector<Point2D> &points)
{
    if (points.size() != scan.ranges.size())
        points.resize(scan.ranges.size());

    for (size_t i = 0; i < scan.ranges.size(); i++) {
        double angle = scan.min_angle + scan.angle_increment * i;
        points[i].x = scan.ranges[i] * cos(angle);
        points[i].y = scan.ranges[i] * sin(angle);
    }
}

} // namespace

std::unique_ptr<SdkCore> SdkCore::Create(const std::vector<std::string> &cfg_path, const std::string &main_file)
{
    if (cfg_path.empty() || main_file.empty()) {
        std::cerr << "Parameter cfg_path or main_file does not exist" << std::endl;
        return nullptr;
    }

    auto configurator = Configurator::Create(cfg_path, main_file);
    if (configurator == nullptr) {
        std::cerr << "Failed to create Configurator" << std::endl;
        return nullptr;
    }

    auto loader = configurator->GetExtractor("filters");
    if (loader == nullptr) {
        std::cerr << "Failed to load filter parameters" << std::endl;
        return nullptr;
    }

    Scripts::Filters params;
    if (!Scripts::GetFiltersOptions(std::move(loader), params))
        return nullptr;

    // 创建离群点滤波器
    Scripts::OFParams of = params.outlier_filter;
    std::unique_ptr<OutlierFilter> outlierfilter = OutlierFilter::Create(of.radius, of.min_count, of.max_distance);
    if (outlierfilter == nullptr) {
        std::cerr << "Failed to create Radius Filter" << std::endl;
        return nullptr;
    }

    // 创建平滑滤波器
    Scripts::SFParams sf = params.smooth_filter;
    std::unique_ptr<Smoother> smoother = Smoother::Create(sf.level, sf.err);
    if (smoother == nullptr) {
        std::cerr << "Failed to Create Smoother" << std::endl;
        return nullptr;
    }

    // 创建拖尾滤波器
    Scripts::TFParams tf = params.trailing_filter;
    std::unique_ptr<TrailingFilter> trailingfilter = TrailingFilter::Create(tf.window, tf.neighbors, tf.min_angle, tf.max_angle);
    if (trailingfilter == nullptr) {
        std::cerr << "Failed to Create TrailingFilter" << std::endl;
        return nullptr;
    }

    loader = configurator->GetExtractor("protocol");
    if (loader == nullptr) {
        std::cerr << "Failed to load protocol parameters" << std::endl;
        return nullptr;
    }

    Scripts::Protocol protocol;
    if (!Scripts::GetProtocol(std::move(loader), protocol))
        return nullptr;

    if (!(protocol.uart.enable ^ protocol.ethernet.enable)) {
        std::cerr << "The communication protocol is not set or set twice" << std::endl;
        return nullptr;
    }

    std::unique_ptr<IWorker> worker = nullptr;
    if (protocol.ethernet.enable) {
        worker = Ethernet::Create(protocol.ethernet.listen_port, protocol.ethernet.radar_ip, protocol.ethernet.radar_port);
    } else {
        worker = Uart::Create(protocol.uart.name, protocol.uart.baudrate);
    }

    if (worker == nullptr) {
        std::cerr << "Failed to Create Ethernet or Uart" << std::endl;
        return nullptr;
    }

    std::unique_ptr<Lidar> lidar = Lidar::Create(std::move(worker));
    if (lidar == nullptr) {
        std::cerr << "Failed to Create Lidar" << std::endl;
        return nullptr;
    }

    loader = configurator->GetExtractor("lidar");
    if (loader == nullptr) {
        std::cerr << "Failed to load lidar parameters" << std::endl;
        return nullptr;
    }

    Scripts::LidarAttribute attr;
    if (!Scripts::GetLidarAttr(std::move(loader), attr))
        return nullptr;

    lidar->SetAttribute(attr);

    if (params.angle_filter.enable)
        lidar->SetRemoved(Scripts::GetRangeWithPose(params.angle_filter.removed, attr.zero_angle));

    std::unique_ptr<SdkCore> sdk(new SdkCore(std::move(lidar)));
    if (sdk == nullptr)
        return nullptr;

    if (of.enable)
        sdk->set_outlier_filter(std::move(outlierfilter));

    if (sf.enable)
        sdk->set_smoother(std::move(smoother));

    if (tf.enable)
        sdk->set_trailing_filter(std::move(trailingfilter));

    return sdk;
}

SdkCore::SdkCore(std::unique_ptr<Lidar> lidar)
    : lidar_(std::move(lidar)), handler_{nullptr}, outlier_filter_(nullptr), smoother_(nullptr),
      trailing_filter_(nullptr)
{
    lidar_->Bind(std::bind(&SdkCore::Handle, this, std::placeholders::_1));
}

SdkCore::~SdkCore() { }

void SdkCore::RegisterDataDistributor(Type type, std::function<void(const LScan &)> handler)
{
    if (static_cast<uint32_t>(type) >= 2) {
        std::cerr << "Failed to Register DataDistributor" << std::endl;
        return;
    }

    handler_[static_cast<uint32_t>(type)] = handler;
}

void SdkCore::RegisterCtlRunHandler(CtlRunHandler handler)
{
    lidar_->SetWorkerCtlRunHandler(handler);
}

void SdkCore::Publish(Type type, const LScan &scan)
{
    if (static_cast<uint32_t>(type) >=  2) {
        std::cerr << "Failed to Publish data to distributor" << std::endl;
        return;
    }

    if (handler_[static_cast<uint32_t>(type)] != nullptr)
        handler_[static_cast<uint32_t>(type)](scan);
}

bool SdkCore::Run(bool detached)
{
    return lidar_->Launch(detached);
}

void SdkCore::Handle(LScan &scan)
{
    this->Publish(Type::Raw, scan);

    LScan new_scan;
    ScanCopy(scan, new_scan);

    LScan new_scan_near;
    ScanCopy(scan, new_scan_near);

    if (smoother_ != nullptr) {
        smoother_->SetData(&scan.ranges);
        smoother_->Run(scan.ranges);
    }

    if (trailing_filter_ != nullptr) {
        std::vector<int> removed;
        trailing_filter_->Run(scan, removed);

        for (auto index : removed)
            scan.ranges[index] = 0;
    }


    if (outlier_filter_ != nullptr) {
        std::vector<Point2D> points;
        SwitchToCartesian(new_scan, points);

        outlier_filter_->SetCloud(&points);

        std::vector<Point2D> handled;
        std::vector<int> removed;
        if (!outlier_filter_->Run(OutlierFilter::Mode::Knn, handled, removed)) {
            std::cerr << "Failed to filter outlier" << std::endl;
            return;
        }

        for (size_t i = 0; i < removed.size(); i++) {
            new_scan.ranges[removed.at(i)] = 0;
            new_scan.intensities[removed.at(i)] = 0;
        }

        for (size_t i = 0; i < scan.ranges.size(); i++) {
            if (0 != new_scan.ranges[i])
                scan.ranges[i] = new_scan.ranges[i];
        }
    }

/*
    if (outlier_filter_ != nullptr) {
        //far
        std::vector<Point2D> points;
        SwitchToCartesian(new_scan, points);

        outlier_filter_->SetCloud(&points);

        std::vector<Point2D> handled;
        std::vector<int> removed;
        if (!outlier_filter_->Run(OutlierFilter::Mode::Knn, handled, removed)) {
            std::cerr << "Failed to filter outlier" << std::endl;
            return;
        }

        for (size_t i = 0; i < removed.size(); i++) {
            new_scan.ranges[removed.at(i)] = 0;
            new_scan.intensities[removed.at(i)] = 0;
        }

        for (size_t i = 0; i < scan.ranges.size(); i++) {
            if( (0 != new_scan.ranges[i]) && (!(new_scan.ranges[i] < 0.3)) )
            {
                scan.ranges[i] = new_scan.ranges[i];
            }
        }

        //near
        double radius_search_bk = outlier_filter_->get_radius_search();
        int min_neighbors_in_radius_bk = outlier_filter_->get_min_neighbors_in_radius();
        double max_distance_bk = outlier_filter_->get_max_distance();
        
        outlier_filter_->set_radius_search(0.006);
        outlier_filter_->set_min_neighbors_in_radius(5);
        outlier_filter_->set_max_distance(0.6);
        
        std::vector<Point2D> points_near;
        SwitchToCartesian(new_scan_near, points_near);

        outlier_filter_->SetCloud(&points_near);

        std::vector<Point2D> handled_near;
        std::vector<int> removed_near;
        if (!outlier_filter_->Run(OutlierFilter::Mode::Knn, handled_near, removed_near)) {
            std::cerr << "Failed to filter outlier" << std::endl;
            return;
        }

        for (size_t i = 0; i < removed_near.size(); i++) {
            new_scan_near.ranges[removed_near.at(i)] = 0;
            new_scan_near.intensities[removed_near.at(i)] = 0;
        }

        for (size_t i = 0; i < scan.ranges.size(); i++) {
            //if( (0 != new_scan_near.ranges[i]) && (new_scan_near.ranges[i] < 0.3) )
            if( (scan.ranges[i] < 0.3) && (new_scan_near.ranges[i] < 0.3) )
            {
                scan.ranges[i] = new_scan_near.ranges[i];
            }
        }

        outlier_filter_->set_radius_search(radius_search_bk);
        outlier_filter_->set_min_neighbors_in_radius(min_neighbors_in_radius_bk);
        outlier_filter_->set_max_distance(max_distance_bk);
    }
*/

    if (smoother_ != nullptr) {
        smoother_->SetData(&scan.ranges);
        smoother_->Run(scan.ranges, scan.intensities);
    }

    this->Publish(Type::Filtered, scan);
}
