//
// Copyright (c) 2022 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#include "node.h"
#include "filters/trailingfilter.h"

#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/LaserScan.h>
#include <assert.h>

#include <map>
#include <algorithm>

#include <std_msgs/String.h>

Node::Node(const std::string &frame_id, bool is_scan)
    : frame_id_(frame_id), is_scan_(is_scan), ctl_run_(true) {}

Node::~Node() {}

std::unique_ptr<Node> Node::Create(const NodeAttribute &attribute)
{
    if (!(attribute.use_polar_coordinates ^ attribute.use_cartesian_cooridnates)) {
        std::cerr << "The point cloud output setting is not set or set twice" << std::endl;
        return nullptr;
    }

    std::unique_ptr<Node> node(new Node(attribute.frame_id,  attribute.use_polar_coordinates));
    if (node == nullptr)
        return nullptr;

    if (!node->Init(attribute.topic, attribute.queue_size))
        return nullptr;

    return node;
}

std::unique_ptr<Node> Node::Create(const NodeAttribute &attribute, int sq)
{
    if (!(attribute.use_polar_coordinates ^ attribute.use_cartesian_cooridnates)) {
        std::cerr << "The point cloud output setting is not set or set twice" << std::endl;
        return nullptr;
    }

    std::unique_ptr<Node> node(new Node(attribute.frame_ids[sq],  attribute.use_polar_coordinates));
    if (node == nullptr)
        return nullptr;

    if (!node->Init(attribute.topics[sq], attribute.queue_size))
        return nullptr;

    return node;
}

bool Node::Init(const std::string &topic, uint32_t queue_size)
{
    if (is_scan_) {
        publisher_ = node_handle_.advertise<sensor_msgs::LaserScan>(topic, queue_size);
    } else {
        publisher_ = node_handle_.advertise<sensor_msgs::PointCloud>(topic, queue_size);
    }

    ctl_subscriber_ = node_handle_.subscribe<std_msgs::String>(topic + "_ctl", 5, std::bind(&Node::CtlHandle, this, std::placeholders::_1));

#ifdef SDK_TEST
    tst_publisher_ = node_handle_.advertise<sensor_msgs::LaserScan>("/orig" + topic, queue_size);
#endif

    return true;
}

void Node::CtlHandle(const std_msgs::String::ConstPtr &ctl)
{
    //std::cout << "CtlHandle: " << ctl->data << std::endl;

    //std::cout << "frame_id_: " << frame_id_ << std::endl;

    if(ctl->data == std::string("start"))
    {
        std::cout << "start lidar" << std::endl;
        ctl_run_ = true;
    }
    else if(ctl->data == std::string("stop"))
    {
        std::cout << "stop lidar" << std::endl;
        ctl_run_ = false;
    }
}

bool Node::GetCtlRun()
{
    return ctl_run_;
}

#ifdef SDK_TEST
void Node::HandleRawData(const LScan &scan)
{
    sensor_msgs::LaserScan orig;
    orig.header.frame_id = frame_id_;
    orig.header.stamp = ros::Time(scan.stamp);
    orig.angle_increment = scan.angle_increment;
    orig.angle_min = scan.min_angle;
    orig.angle_max = scan.max_angle;
    orig.range_min = scan.min_range;
    orig.range_max = scan.max_range;

    orig.ranges.resize(scan.ranges.size());
    orig.intensities.resize(scan.intensities.size());
    for (size_t i = 0; i < scan.ranges.size(); i++) {
        orig.ranges[i] = scan.ranges[i];
        orig.intensities[i] = scan.intensities[i];
    }

    tst_publisher_.publish(orig);
}
#endif

void Node::HandleFilteredData(const LScan &scan)
{
    sensor_msgs::LaserScan orig;
    orig.header.frame_id = frame_id_;
    orig.header.stamp = ros::Time(scan.stamp);
    orig.angle_increment = scan.angle_increment;
    orig.angle_min = scan.min_angle;
    orig.angle_max = scan.max_angle;
    orig.range_min = scan.min_range;
    orig.range_max = scan.max_range;

    orig.ranges.resize(scan.ranges.size());
    orig.intensities.resize(scan.intensities.size());
    for (size_t i = 0; i < scan.ranges.size(); i++) {
        orig.ranges[i] = scan.ranges[i];
        orig.intensities[i] = scan.intensities[i];
    }

    publisher_.publish(orig);
}
