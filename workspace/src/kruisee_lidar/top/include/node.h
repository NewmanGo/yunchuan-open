//
// Copyright (c) 2022 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#pragma once

#include <ros/ros.h>

#include <string>
#include <memory>
#include <tuple>

#include "common/scripts.h"
#include "drivers/msg.h"
#include "filters/ismoother.h"
#include "filters/outlierfilter.h"

#include <std_msgs/String.h>

struct NodeAttribute {
    std::string topic;
    std::vector<std::string> topics;
    int queue_size;
    std::string frame_id;
    std::vector<std::string> frame_ids;
    bool use_polar_coordinates;
    bool use_cartesian_cooridnates;
};

class Node {
public:
    static std::unique_ptr<Node> Create(const NodeAttribute &attribute);
    static std::unique_ptr<Node> Create(const NodeAttribute &attribute, int sq);
    ~Node();

#ifdef SDK_TEST
    void HandleRawData(const LScan &scan);
#endif

    void HandleFilteredData(const LScan &scan);

    bool GetCtlRun();

private:
    Node(const std::string &frame_id, bool is_scan);
    bool Init(const std::string &topic, uint32_t queue_size);
    void CtlHandle(const std_msgs::String::ConstPtr &ctl);

private:
    ros::NodeHandle node_handle_;
    ros::Publisher publisher_;
    ros::Subscriber ctl_subscriber_;

    bool ctl_run_;

#ifdef SDK_TEST
    ros::Publisher tst_publisher_;
#endif

    std::string frame_id_;
    bool is_scan_;
};