//
// Copyright (c) 2022 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#pragma once

#define NO_ERROR            (0)
#define ERR_NOT_MEMORY      (-1)
#define ERR_INVALID_ARGS    (-2)
#define ERR_NOT_FOUND       (-3)
#define ERR_NOT_LAUNCH      (-4)