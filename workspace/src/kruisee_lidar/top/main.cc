//
// Copyright (c) 2022 ECOVACS
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT
//

#include <ros/ros.h>
#include <ros/package.h>

#include <memory>
#include <string>
#include <iostream>

#include "common/loader.h"
#include "common/scripts.h"
#include "common/configurator.h"
#include "drivers/uart.h"
#include "drivers/ethernet.h"
#include "drivers/lidar.h"
#include "filters/outlierfilter.h"
#include "filters/smoother.h"

#include "sdkcore.h"

#include "node.h"
#include "err.h"

#include "spdlog/logger.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/rotating_file_sink.h"

namespace {

bool GetNodeAttribute(NodeAttribute &attribute, std::string &sdk_core_path)
{
    ros::NodeHandle nh("kruisee_lidar_node");
    if (!nh.getParam("sdk_core_path", sdk_core_path)) {
        std::cerr << "Failed to load sdk core path" << std::endl;
        return false;
    }

    if (!nh.getParam("topic", attribute.topic)) {
        std::cerr << "Failed to load ros topic name" << std::endl;
        return false;
    }

    if (!nh.getParam("topics", attribute.topics)) {
        std::cerr << "Failed to load ros topics name" << std::endl;
        return false;
    }

    if (!nh.getParam("queue_size", attribute.queue_size)) {
        std::cerr << "Failed to load ros topic queue size value" << std::endl;
        return false;
    }

    if (attribute.queue_size < 0) {
        std::cerr << "Illegal data value" << std::endl;
        return false;
    }

    if (!nh.getParam("frame_id", attribute.frame_id)) {
        std::cerr << "Failed to load ros topic frame id" << std::endl;
        return false;
    }

    if (!nh.getParam("frame_ids", attribute.frame_ids)) {
        std::cerr << "Failed to load ros topic frame ids" << std::endl;
        return false;
    }

    if (!nh.getParam("use_cartesian_cooridnates", attribute.use_cartesian_cooridnates)) {
        std::cerr << "Failed to load use_cartesian_cooridnates flag" << std::endl;
        return false;
    }

    if (!nh.getParam("use_polar_coordinates", attribute.use_polar_coordinates)) {
        std::cerr << "Failed to load use_polar_coordinates flag" << std::endl;
        return false;
    }

    if (!(attribute.use_polar_coordinates ^ attribute.use_cartesian_cooridnates)) {
        std::cerr << "The pointcloud type is not set or set twice" << std::endl;
        return false;
    }

    return true;
}

// 预定义SDK CORE 配置文件路径
const std::string kSDKCOREConfigDefaultPath{ SDK_CFG_FILES_DIR };

} // namespace

void test_log()
{
    /*
    try 
    {
        std::cout << "Log0" << std::endl;
        auto file_logger = spdlog::rotating_logger_mt("krusee_lidar", "log/test_log", 
                                                      1024 * 1024 * 5, 10);
	file_logger->set_level(spdlog::level::info);
	
	std::cout << "Log1" << std::endl;
	
        int i = 0;
        while (i < 1000000)
        {
            file_logger->info("pub message #{}", i);
            i++;
        }
        
        std::cout << "Log2" << std::endl;
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
    }
    */
    try 
    {
        //std::cout << "Log0" << std::endl;
        std::shared_ptr<spdlog::logger> file_logger = spdlog::rotating_logger_mt("krusee_lidar", "/work/catkin_workspace_log/log/test_log", 
                                                      1024 * 1024 * 5, 10);
	    file_logger->set_level(spdlog::level::info);
        
        spdlog::flush_on(spdlog::level::info);
	
	//std::cout << "Log1" << std::endl;
	
        int i = 0;
        //while (i < 1000000)
        while (i < 100)
        {
            file_logger->info("pub message #{}", i);
            i++;
        }
        
        //std::cout << "Log2" << std::endl;
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
    }   
    
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "kruisee_lidar_node");

    //std::cout << "kruisee_lidar path: " << ros::package::getPath("kruisee_lidar") << std::endl;

    std::string sdk_core_path;
    NodeAttribute attribute;
    if (!GetNodeAttribute(attribute, sdk_core_path)) {
        std::cerr << "Failed to get ros node attribute" << std::endl;
        return ERR_INVALID_ARGS;
    }

    PubLog::log = spdlog::rotating_logger_mt("krusee_lidar", sdk_core_path+"/log/pub_log", 1024 * 1024 * 2, 10);
    PubLog::log->set_level(spdlog::level::info);
    spdlog::flush_on(spdlog::level::info);

    std::unique_ptr<SdkCore> sdkcore[255];
    std::unique_ptr<Node> node[255];

    for(size_t i = 0; i < attribute.topics.size(); i++)
    {
        std::string cfg_file(CFG_FILE);
        std::size_t pos = cfg_file.find(".lua");
        cfg_file.erase(pos, 4);
        cfg_file += "_";
        cfg_file += std::to_string(i);
        cfg_file += ".lua";
        std::cout << "cfg_file name is:" << cfg_file << std::endl;

        //auto sdkcore = SdkCore::Create(std::vector<std::string>{ kSDKCOREConfigDefaultPath, sdk_core_path.append("/config") }, CFG_FILE);

        //auto sdkcore = SdkCore::Create(std::vector<std::string>{ kSDKCOREConfigDefaultPath, sdk_core_path.append("/config") }, cfg_file);
        sdkcore[i] = SdkCore::Create(std::vector<std::string>{ kSDKCOREConfigDefaultPath, sdk_core_path.append("/config") }, cfg_file);

        if (sdkcore[i] == nullptr) {
        //if (sdkcore == nullptr) {
            std::cout << "Failed to create sdk core" << std::endl;
            return ERR_NOT_LAUNCH;
        }

        //auto node = Node::Create(attribute, i);

        node[i] = Node::Create(attribute, i);
        if (node[i] == nullptr) {
        //if (node == nullptr) {
            std::cerr << "Failed to create ROS Node" << std::endl;
            return ERR_NOT_MEMORY;
        }

    #ifdef SDK_TEST
        sdkcore->RegisterDataDistributor(SdkCore::Raw, std::bind(&Node::HandleRawData, node.get(), std::placeholders::_1));
    #endif

        //sdkcore->RegisterDataDistributor(SdkCore::Filtered, std::bind(&Node::HandleFilteredData, node.get(), std::placeholders::_1));

        //sdkcore->Run();

        sdkcore[i]->RegisterCtlRunHandler(std::bind(&Node::GetCtlRun, node[i].get()));

        sdkcore[i]->RegisterDataDistributor(SdkCore::Filtered, std::bind(&Node::HandleFilteredData, node[i].get(), std::placeholders::_1));

        sdkcore[i]->Run();

    }




    ros::spin();
	return NO_ERROR;
}