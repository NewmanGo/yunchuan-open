# KRUISEE SDK

## 1 简介

KRUISEE SDK 是为 KRUISEE 公司旗下所有产品设计的软件开发套件。它是基于C++11进行开发，对底层差异化通信进行封装和点云处理进行抽象，并为用户提供易用的接口。通过KRUISEE SDK，用户可以快速地连接 KRUISEE 产品并接收点云。

## 2 开发环境
+ Ubuntu 14.04/Ubuntu 16.04/Ubuntu 18.04/Ubuntu 20.04 ROS
+ Windows 10
+ C++11 compiler
+ cmake (version >= 2.8.3)

## 3 通信协议
| 字段 | 偏移（字节）| 大小（字节）| 描述 |
|:--------:|:--------:|:--------:|:--------:|
| header | 0 | 1 | 帧头，固定为0xFA|
| angle | 1 | 1 | 起始角度(实际角度=angle-0xA0) |
| speed | 2 | 2 | 雷达转速 |
| ranges | 4 | 16 * 2 | 测距值(单个值占2bytes)|
| intensities | 36 | 16 | 信号强度 |
| timestamp | 52 | 4 | 时间戳 |
| cksum | 56 | 2 | 校验值 |

## 4 使用
### 4.1 Ubuntu 14.04/Ubuntu 16.04/Ubuntu 18.04/Ubuntu 20.04 ROS
#### 依赖
KRUISEE SDK 需要一些编译工具和相关库。你可以通过 apt 工具安装这些依赖：
```shell
# [1] 安装cmake
sudo apt install cmake
```

#### ROS构建 & 编译
```shell

# [1] 将 kruisee_lidar 目录拷贝到ROS工作目录下
cp {kruisee_lidar folder}/kruisee_lidar {ROS Workspace folder}/src/

# [2] 编译SDK
cd {ROS Workspace folder}
catkin_make

# [3] 检查系统网络环境
# 检测当前环境是否处在 169.254.118.x 网段(默认情况)，没有则需要进行网络配置(确保PC端和模组处在同一网段)

# [4] 运行
支持多个雷达，修改kruisee_lidar.launch
每个雷达点云的topic，frame_ids，按照如下修改：
<rosparam param="topics">["scan","scan1"]</rosparam>
<rosparam param="frame_ids">["map","map1"]</rosparam>
对于每个雷达，需创建一个kruisee_lidar_x.lua文件，可以复制kruisee_lidar_t.lua后改名，具体含义参考5配置使用，x从0开始，kruisee_lidar_0.lua kruisee_lidar_1.lua ...

source {kruisee_lidar folder}/devel/setup.sh
roslaunch kruisee_lidar kruisee_lidar.launch

```

### 4.2 Ubuntu 14.04/Ubuntu 16.04/Ubuntu 18.04/Ubuntu 20.04 NONE ROS
后续更新

### 4.3 Windows 10
后续更新

## 5 配置使用

用户尽量不要直接修改如下配置文件: filters.lua、platform.lua、pose.lua、protocol.lua
用户可以修改kruisee_lidar_x.lua或者自定义配置脚本
修改其中的ETHERNET.radar_ip = "169.254.119.2"，每个雷达的地址注意实现设置为不同；

用户可以在 return options 前进行SDK配置
| 配置 | 描述 | 注意 |
|:---:|:----:|:----:|
| PROTOCOL.use_net_protocol = true | 选择以太网通信协议 |
| PROTOCOL.use_uart_protocol = true | 选择串口通信协议 | 以太网和串口只能选择其一 |
| FILTERS.use_radius_filter = true | 开启半径滤波 |
| FILTERS.use_smooth_filter = true | 开启平滑滤波 |
| FILTERS.use_angle_filter = true | 开启角度滤波 |
| ANGLE_FILTER.range = { { 0, 45 }, {90, 270} } | 滤除0-45, 90-270度的点云数据 |
| PLATFORM.use_polar_coordinates = true | 以极坐标方式输出点云, ROS下为sensor_msgs::LaserScan |
| PLATFORM.use_cartesian_coordinates = true | 以直角坐标系输出点云,ROS下为sensor_msgs::PointCloud |
| POSE.rotate_angle = 123.0 | 设置零度，相当于机械零度逆时针旋转 |
| ETHERNET.radar_ip = "169.254.119.2" | 设置雷达IP |
| UART.name = "/dev/ttyUSB0" | 设置串口号 |
| RADIUS_FILTER.radius = 0.15 | 设置半径滤波器滤波半径 |
| RADIUS_FILTER.min_count = 4 | 设置半径滤波器最小临近点数 |
| SMOOTH_FILTER.level = 3 | 设置平滑滤波器平滑等级，越大滤波程度越高 |
| SMOOTH_FILTER.err = 0.1 |设置平滑滤波器滤波误差，越大滤除点越多 |

基于lua的配置文件解释如下

```lua
--
-- Copyright (c) 2022 ECOVACS
--
-- Use of this source code is governed by a MIT-style
-- license that can be found in the LICENSE file or at
-- https://opensource.org/licenses/MIT
--

-- 包含通信协议的脚本
include "protocol.lua"

-- 包含位姿相关的脚本
include "pose.lua"

-- 包含滤波器相关的脚本
include "filters.lua"

-- 包含平台相关的脚本
include "platform.lua"

-- options 禁止修改
options = {
    -- 导入通信协议
    protocol = PROTOCOL,

    -- 导入滤波器集合
    filters = FILTERS,

    -- 导入激光雷达位姿相关配置
    pose = POSE,

    -- 导入平台依赖配置(例如ROS)
    platform = PLATFORM,
}

ETHERNET.radar_ip = "169.254.119.2"

-- 此处可以修改配置信息，使用如些所示

-- [1] 选择通信方式，二选一将使用的注释掉或者改为false
PROTOCOL.use_net_protocol = true    -- 采用以太网通信(默认开启)
PROTOCOL.use_uart_protocol = false  -- 采用串口通信(默认关闭)

-- [2] 设置雷达零度位置，相当于机械零度逆时针旋转指定角度
POSE.rotate_angle = 123.0

-- [3] 设置半径滤波
RADIUS_FILTER.radius = 0.15         -- 设置滤波半径
RADIUS_FILTER.min_count = 4         -- 设置滤波点数，在滤波半径内点数小于该值将会该滤除
RADIUS_FILTER.max_distance = 5      -- 设置半径滤波最大范围，单位m
FILTERS.use_radius_filter = true    -- 开启半径滤波

-- [4] 设置平滑滤波
SMOOTH_FILTER.level = 1             -- 设置平滑等级
SMOOTH_FILTER.err =0.1              -- 设置平滑滤波误差，小于该值进行滤除
FILTERS.use_smooth_filter = true    -- 开启平滑滤波

-- [5] 设置角度滤波
ANGLE_FILTER.range = { {90, 270} }  -- 设置角度滤波范围，角度范围相当于设置零度位置后的角度范围
FILTERS.use_angle_filter = true     -- 开启角度滤波

-- 默认点云极坐标系输出
PLATFORM.use_polar_coordinates = true

return options
```

## 6 NOTICE

当前版本滤波算法需要用户安装flann库和lz4库, 具体方式:
修改CMakeList.txt => target_link_libraries下libflann.so, libflann_cpp.so, liblz4.a