//
// Created by huangcaibi on 2022/11/18.
//
#include "nmea_comms/rtk_socket.h"

RtkSocket::RtkSocket(ros::NodeHandle &n, ros::NodeHandle &n_local) : n_(n), n_local_(n_local) {
  initConfig();
  nmea_gga_ = "";
  update_yaw_time_ = ros::Time::now().toSec();

  n_local_.param<std::string>("frame_id", frame_id_, "navsat");
  // 从robot.json中读取
  // n_local_.param<std::string>("server_name", server_name_, "");
  // n_local_.param("port", port_, 8002);
  // n_local_.param<std::string>("user_name", user_name_, "");
  // n_local_.param<std::string>("password", password_, "");
  // n_local_.param<std::string>("mount", mount_, "");

  n_local_.param("angle_based_mode", angle_based_mode_, false);
  n_local_.param("send_gga_time_interval", send_gga_time_interval_, 60);
  n_local_.param<std::string>("preinstall_gga_msg", preinstall_gga_msg_, "");

  ROS_INFO("[SOCKET]Server Name: %s", server_name_.c_str());
  ROS_INFO("[SOCKET]Server Port: %d", port_);
  ROS_INFO("[SOCKET]User Name: %s", user_name_.c_str());
  ROS_INFO("[SOCKET]Password: %s", password_.c_str());

  rtcm_pub_ = n_.advertise<mavros_msgs::RTCM>("/rtcm", 5);
  gnss_pub_ = n_.advertise<sensor_msgs::NavSatFix>("/gnss", 5);

  sub_ = n_.subscribe<nmea_msgs::Sentence>("/nmea_sentence", 5, &RtkSocket::nmea_sentence_callback, this);

  boost::function0<void> s_f = boost::bind(&RtkSocket::_socket_thread_func, this);
  boost::thread thrd(s_f);

#ifdef SAVE_GNSS_DATA
  gnss_write.open("/home/huangcaibi/gnss.txt", std::ios::app);
#endif
}

RtkSocket::~RtkSocket() {
#ifdef SAVE_GNSS_DATA
  gnss_write.close();
#endif
}

void RtkSocket::initConfig() {
    // 打开JSON配置文件
    std::ifstream f("/home/firefly/config/robot.json");
    json config_data;

    try {
      config_data = json::parse(f);
    } catch(std::exception& e) {
      ROS_ERROR("config parse error");
      f.close();
      return;
    }

    f.close();

    server_name_ = config_data["gps"]["server_name"];
    port_ = config_data["gps"]["port"];
    user_name_ = config_data["gps"]["username"];
    password_ = config_data["gps"]["password"];
    mount_ = config_data["gps"]["mount"];
}

void RtkSocket::nmea_sentence_callback(const nmea_msgs::SentenceConstPtr &nmea_sentence_ptr) {
#ifdef SAVE_GNSS_DATA
  gnss_write << nmea_sentence_ptr->sentence << std::endl;
#endif
  if (strstr(nmea_sentence_ptr->sentence.c_str(), "$GNGGA")) {
    Location location = GNGGA_decode(nmea_sentence_ptr->sentence);
    if (location.lat == "" || location.lon == "") {
      if (!angle_based_mode_) {
        ROS_INFO("[SOCKET]Don't Have Location Info: %s", nmea_sentence_ptr->sentence.c_str());
      }
      return;
    } else {
      if (!angle_based_mode_) {
        ROS_INFO("[SOCKET]Send GNSS Info: %s", nmea_sentence_ptr->sentence.c_str());
      }
    }
    nmea_gga_ = nmea_sentence_ptr->sentence;
    sensor_msgs::NavSatFix sat = navsatfix_encode(location);
    sat.header.frame_id = nmea_sentence_ptr->header.frame_id;
    mtx.lock();
    sat_ = sat;
    update_sat_time_ = ros::Time::now().toSec();
    mtx.unlock();
    if (!angle_based_mode_) {
      gnss_pub_.publish(sat);
    }
  } else if (strstr(nmea_sentence_ptr->sentence.c_str(), "$GNHPR")) {
    double yaw = GNHPR_decode(nmea_sentence_ptr->sentence);
    if (yaw == -999999 || yaw == 0) {
      ROS_INFO("[SOCKET]Can't Save GNSS Yaw Info: %s", nmea_sentence_ptr->sentence.c_str());
    } else {
      ROS_INFO("[SOCKET]Save GNSS Yaw Info: %s", nmea_sentence_ptr->sentence.c_str());
      gnss_yaw_ = yaw;
      update_yaw_time_ = ros::Time::now().toSec();
      if (angle_based_mode_ && fabs(update_yaw_time_ - update_sat_time_) < 0.3) {
        mtx.lock();
        sat_.altitude = gnss_yaw_;
        gnss_pub_.publish(sat_);
        mtx.unlock();
      }
    }
  } else if (strstr(nmea_sentence_ptr->sentence.c_str(), "$KSXT")) {
    Location location = KSXT_decode(nmea_sentence_ptr->sentence);
    if (location.lat == "" || location.lon == "" || location.lat == "0.00000000" || location.lat == "0.00000000") {
      ROS_INFO("[SOCKET]Don't Have KSXT Info: %s", nmea_sentence_ptr->sentence.c_str());
      return;
    } else {
      ROS_INFO("[SOCKET]Send KSXT Info: %s", nmea_sentence_ptr->sentence.c_str());
    }
    sensor_msgs::NavSatFix sat = navsatfix_KSXT_encode(location);
    sat.header.frame_id = nmea_sentence_ptr->header.frame_id;
    gnss_pub_.publish(sat);
  } else if (strstr(nmea_sentence_ptr->sentence.c_str(), "$GPGGA")) {
    Location location = GPGGA_decode(nmea_sentence_ptr->sentence);
    if (location.lat == "" || location.lon == "") {
      ROS_INFO("[SOCKET]Don't Have Location Info: %s", nmea_sentence_ptr->sentence.c_str());
      return;
    } else {
      ROS_INFO("[SOCKET]Send GNSS Info: %s", nmea_sentence_ptr->sentence.c_str());
    }
    nmea_gga_ = nmea_sentence_ptr->sentence;
    sensor_msgs::NavSatFix sat = navsatfix_encode(location);
    if (ros::Time::now().toSec() - update_yaw_time_ < 1.2) {
      sat.altitude = gnss_yaw_;
    } else {
      sat.altitude = -999999;
    }
    sat.header.frame_id = nmea_sentence_ptr->header.frame_id;
    gnss_pub_.publish(sat);
  } else if (strstr(nmea_sentence_ptr->sentence.c_str(), "$GPTRA")) {
    double yaw = GPTRA_decode(nmea_sentence_ptr->sentence);
    if (yaw == -999999 || yaw == 0) {
      ROS_INFO("[SOCKET]Can't Save GNSS Yaw Info: %s", nmea_sentence_ptr->sentence.c_str());
    } else {
      ROS_INFO("[SOCKET]Save GNSS Yaw Info: %s", nmea_sentence_ptr->sentence.c_str());
      gnss_yaw_ = yaw;
      update_yaw_time_ = ros::Time::now().toSec();
    }
  }
}

bool send_gga_2_server(int sockfd, std::string nmea_gga) {
  char buf_temp[MAXDATASIZE];
  int i = snprintf(buf_temp, MAXDATASIZE - 40, "Ntrip-GGA: %s\r\n", nmea_gga.c_str());
  if (send(sockfd, buf_temp, (size_t) i, 0) != i) {
    ROS_INFO("[SOCKET]Send Message To Server Error: \n%s", buf_temp);
    return false;
  } else {
    ROS_INFO("[SOCKET]Send Message To Server Success: \n%s", buf_temp);
    return true;
  }
}

void RtkSocket::_socket_thread_func() {
//  /* get server ip */
//  struct hostent *server_ip = NULL;
//  while (NULL == server_ip) {
//    ROS_INFO("[SOCKET]Try to Get The Server Ip");
//    server_ip = gethostbyname(server_name_.c_str());
//    boost::this_thread::sleep(boost::posix_time::seconds(2));
//  }
//
//  /* Initialize socket structure */
//  struct sockaddr_in serv_addr, cli_addr;
//  bzero((char *) &serv_addr, sizeof(serv_addr));
//  serv_addr.sin_family = AF_INET;
//  serv_addr.sin_addr = *((struct in_addr *) server_ip->h_addr);
//  serv_addr.sin_port = htons(port_);

  int sockfd;
  char send_buf[MAXDATASIZE];
  int recv_buf_size;
  char recv_buf[MAXDATASIZE];

  nmea_gga_ = preinstall_gga_msg_;
  while (nmea_gga_ == "") {
    ROS_FATAL("[SOCKET]Wait For GNGGA");
    boost::this_thread::sleep(boost::posix_time::seconds(2));
  }

  //temp 用于验证掉线次数
  int lost_count = 0;

  boost::this_thread::sleep(boost::posix_time::seconds(120));
    
  while (ros::ok()) {
    struct hostent *server_ip = NULL;
    struct sockaddr_in serv_addr, cli_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));

//    if (sockfd) {
      close(sockfd);
      ROS_WARN("[SOCKET][%s] Socket Closed", user_name_.c_str());

      ROS_WARN("[SOCKET][%s] Socket Restart", user_name_.c_str());
//      server_ip = NULL;
      while (NULL == server_ip) {
        ROS_INFO("[SOCKET]Try to Get The Server Ip");
        server_ip = gethostbyname(server_name_.c_str());
        boost::this_thread::sleep(boost::posix_time::seconds(2));
      }

      /* Initialize socket structure */
//      struct sockaddr_in serv_addr, cli_addr;
//      bzero((char *) &serv_addr, sizeof(serv_addr));
      serv_addr.sin_family = AF_INET;
      serv_addr.sin_addr = *((struct in_addr *) server_ip->h_addr);
      serv_addr.sin_port = htons(port_);

      boost::this_thread::sleep(boost::posix_time::seconds(5));
//    }

    ROS_WARN("[SOCKET]Socket Start");
    /* set socketfd */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      ROS_FATAL("[SOCKET]ERROR Opening Socket");
      continue;
    }
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      ROS_ERROR("[SOCKET]Socket Connect Error: %s(errno: %d)", strerror(errno), errno);
      continue;
    } else {
      //连接时的等待时间设置为30秒。
      struct timeval timeout = {30, 0};
      setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *) &timeout, sizeof(struct timeval));
      setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(struct timeval));
      ROS_INFO("[SOCKET]Socket Connect Success");
    }

    int send_buf_size = upload_msg_encode(nmea_gga_, server_name_, user_name_, password_, mount_, send_buf);
    if (send_buf_size <= 0) {
      continue;
    }
    if (send(sockfd, send_buf, (size_t) send_buf_size, 0) != send_buf_size) {
      ROS_INFO("[SOCKET]Send Message To Server Error: \n%s", send_buf);
      continue;
    } else {
      ROS_INFO("[SOCKET]Send Message To Server Success: \n%s", send_buf);
    }
    /* Receive message */
    if ((recv_buf_size = recv(sockfd, recv_buf, MAXDATASIZE - 1, 0)) > 0) {
      if (!strstr(recv_buf, "ICY 200 OK")) {
        ROS_ERROR("[SOCKET]Respond Error: \n%s", recv_buf);
        continue;
      }
      ROS_INFO("[SOCKET]Server Respond: \n%s", recv_buf);
    } else {
      ROS_ERROR("[SOCKET]No Respond");
      continue;
    }
    ROS_WARN("[SOCKET]Start Socket Success!");

    //和连接的时候的超时设置不同。RTCM报文10秒没有来就停止。
    {
      struct timeval timeout = {10, 0};
      setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *) &timeout, sizeof(struct timeval));
      setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(struct timeval));
    }

    if (!send_gga_2_server(sockfd, nmea_gga_)) {
      continue;
    }
    ROS_WARN("[SOCKET]Send Init GNGGA Success!");

    while ((recv_buf_size = recv(sockfd, recv_buf, MAXDATASIZE - 1, 0)) > 0) {
//      ROS_INFO("[SOCKET] %s", recv_buf);
//      ROS_INFO("[SOCKET][%s] rtcm_buf_size: %d", user_name_.c_str(), recv_buf_size);
      mavros_msgs::RTCM rtcm_msg;
      rtcm_msg.header.stamp = ros::Time::now();
      rtcm_msg.header.frame_id = frame_id_;
      rtcm_msg.data.resize(recv_buf_size);
      for (int i = 0; i < recv_buf_size; i++) {
        rtcm_msg.data[i] = recv_buf[i];
      }
      static double last_pub_time = ros::Time::now().toSec();
      last_recv_time_ = last_pub_time;
      if (ros::Time::now().toSec() - last_pub_time > 0.5) {
        rtcm_pub_.publish(rtcm_msg);
        last_pub_time = ros::Time::now().toSec();
      }

      static double last_send_gga_time = ros::Time::now().toSec();
      if (ros::Time::now().toSec() - last_send_gga_time > send_gga_time_interval_) {
        if (!send_gga_2_server(sockfd, nmea_gga_)) {
          break;
        }
        last_send_gga_time = ros::Time::now().toSec();
      }

      //temp
      ROS_INFO("[SOCKET][%s] Lost Connect Count: %d", user_name_.c_str(), lost_count);
    }
    //temp
    lost_count++;
  }
  close(sockfd);
}

