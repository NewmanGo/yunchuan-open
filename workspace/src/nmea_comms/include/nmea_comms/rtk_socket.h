//
// Created by huangcaibi on 2022/11/18.
//

#ifndef CODE_RTK_SOCKET_H
#define CODE_RTK_SOCKET_H

#include "ros/ros.h"
#include "nmea_msgs/Sentence.h"
#include "mavros_msgs/RTCM.h"

#include "nmea_comms/rx.h"
#include "nmea_comms/tx.h"
#include "nmea_comms/codec.h"

#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

#include <netdb.h>
#include <arpa/inet.h>
#include <fstream>
#include <mutex>
#include <fstream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

//#define SAVE_GNSS_DATA

class RtkSocket {
public:
    RtkSocket(ros::NodeHandle &n, ros::NodeHandle &n_local);

    ~RtkSocket();

private:
    ros::NodeHandle n_;
    ros::NodeHandle n_local_;

    ros::Publisher rtcm_pub_;
    ros::Publisher gnss_pub_;
    ros::Publisher gptra_pub_;
    ros::Subscriber sub_;

    std::string frame_id_;
    std::string user_name_, password_, server_name_;
    int port_;

    std::string mount_;

    std::string nmea_gga_;
    double last_recv_time_ = -1;

    int send_gga_time_interval_;
    std::string preinstall_gga_msg_;

    double gnss_yaw_ = -999999;
    double update_yaw_time_;

    sensor_msgs::NavSatFix sat_;
    double update_sat_time_;
    bool angle_based_mode_ = false;

    std::mutex mtx;

    void initConfig();
    void nmea_sentence_callback(const nmea_msgs::SentenceConstPtr &nmea_sentence_ptr);

    void _socket_thread_func();

#ifdef SAVE_GNSS_DATA
    std::ofstream gnss_write;
#endif
};

#endif //CODE_RTK_SOCKET_H
