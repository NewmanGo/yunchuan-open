//
// Created by huangcaibi on 2022/11/18.
//

#ifndef CODE_GNGGA_DECODE_H
#define CODE_GNGGA_DECODE_H

#include "ros/ros.h"
#include "sensor_msgs/NavSatFix.h"

#include <regex>
#include <stdlib.h>

#define MAXDATASIZE 4096

#define STATUS_NO_FIX -1
#define STATUS_NOT 0
#define STATUS_SINGLE 1
#define STATUS_DGPS 2
#define STATUS_FIX 4
#define STATUS_FLOAT 5

#define SERVICE_GPS 1
#define COVARIANCE_TYPE_DIAGONAL_KNOWN 2

#define PI 3.1415926

struct Location {
    std::string nmea;
    std::string lat;
    std::string lon;
    std::string alt;
    std::string hdop;
    std::string vdop;
    std::string fix;
    std::string yaw;
    std::string yaw_fix;
};

Location GNGGA_decode(std::string g_nmea) {
  Location location;
  std::smatch m;
  std::regex e("\\$GNGGA,(.*?),(.*?),.*?,(.*?),.*?,(.*?),.*?,(.*?),(.*?),");
  if (std::regex_search(g_nmea, m, e)) {
//    for (auto x:m) std::cout << x << "^_^ ";
//    std::cout << std::endl;
//    std::cout << "Lat:" << m[2] << "Longi:" << m[3] << std::endl;
    location.nmea = g_nmea;
    location.lat = m[2];
    location.lon = m[3];
    location.fix = m[4];
    location.hdop = m[5];
    location.alt = m[6];
    return location;
  } else
    return location;
}

Location GPGGA_decode(std::string g_nmea) {
  Location location;
  std::smatch m;
  std::regex e("\\$GPGGA,(.*?),(.*?),.*?,(.*?),.*?,(.*?),.*?,(.*?),(.*?),");
  if (std::regex_search(g_nmea, m, e)) {
//    for (auto x:m) std::cout << x << "^_^ ";
//    std::cout << std::endl;
//    std::cout << "Lat:" << m[2] << "Longi:" << m[3] << std::endl;
    location.nmea = g_nmea;
    location.lat = m[2];
    location.lon = m[3];
    location.fix = m[4];
    location.hdop = m[5];
    location.alt = m[6];
    return location;
  } else
    return location;
}

double GPTRA_decode(std::string g_nmea) {
  double yaw_temp;
  double yaw = -999999;
  std::smatch m;
  std::regex e("\\$GPTRA,(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),");
  if (std::regex_search(g_nmea, m, e)) {
//    for (auto x:m) std::cout << x << "^_^ ";
//    std::cout << std::endl;
//    std::cout << "Lat:" << m[2] << "Longi:" << m[3] << std::endl;
    yaw_temp = stod(m[2]);
    //temp
//    yaw = yaw_temp / 180 * PI;
    yaw = yaw_temp;
    return yaw;
  } else
    return yaw;
}

double GNHPR_decode(std::string g_nmea) {
  double yaw_temp;
  double yaw = -999999;
  std::smatch m;
  std::regex e("\\$GNHPR,(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),");
  if (std::regex_search(g_nmea, m, e)) {
//    for (auto x:m) std::cout << x << "^_^ ";
//    std::cout << std::endl;
//    std::cout << "Lat:" << m[2] << "Longi:" << m[3] << std::endl;
    yaw_temp = stod(m[2]);
    //temp
//    yaw = yaw_temp / 180 * PI;
    yaw = yaw_temp;
    return yaw;
  } else
    return yaw;
}

Location KSXT_decode(std::string g_nmea) {
  Location location;
  std::smatch m;
  std::regex e("\\$KSXT,(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),");
  if (std::regex_search(g_nmea, m, e)) {
//    for (auto x:m) std::cout << x << "^_^ ";
//    std::cout << std::endl;
//    std::cout << "Lat:" << m[2] << "Longi:" << m[3] << std::endl;
    location.nmea = g_nmea;
    location.lon = m[2];
    location.lat = m[3];
    location.yaw = m[5];

    location.fix = m[10];
    location.yaw_fix = m[11];

    return location;
  } else
    return location;
}

static const char encodingTable[64] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};

/* does not buffer overrun, but breaks directly after an error */
/* returns the number of required bytes */
static int user_pwd_encode(char *buf, int size, const char *user, const char *pwd) {
  unsigned char inbuf[3];
  char *out = buf;
  int i, sep = 0, fill = 0, bytes = 0;

  while (*user || *pwd) {
    i = 0;
    while (i < 3 && *user) inbuf[i++] = *(user++);
    if (i < 3 && !sep) {
      inbuf[i++] = ':';
      ++sep;
    }
    while (i < 3 && *pwd) inbuf[i++] = *(pwd++);
    while (i < 3) {
      inbuf[i++] = 0;
      ++fill;
    }
    if (out - buf < size - 1)
      *(out++) = encodingTable[(inbuf[0] & 0xFC) >> 2];
    if (out - buf < size - 1)
      *(out++) = encodingTable[((inbuf[0] & 0x03) << 4)
                               | ((inbuf[1] & 0xF0) >> 4)];
    if (out - buf < size - 1) {
      if (fill == 2)
        *(out++) = '=';
      else
        *(out++) = encodingTable[((inbuf[1] & 0x0F) << 2)
                                 | ((inbuf[2] & 0xC0) >> 6)];
    }
    if (out - buf < size - 1) {
      if (fill >= 1)
        *(out++) = '=';
      else
        *(out++) = encodingTable[inbuf[2] & 0x3F];
    }
    bytes += 4;
  }
  if (out - buf < size)
    *out = 0;
  return bytes;
}

int upload_msg_encode(std::string nmea_gga, std::string server_name,
                      std::string user_name, std::string password, std::string mount, char *buf) {
//  int i = snprintf(buf, MAXDATASIZE - 40, /* leave some space for login */
//                   "GET /%s HTTP/1.1\r\n"
//                   "Host: %s\r\n"
//                   "User-Agent: NTRIP NtripClientPOSIX/$Revision: 1.51 $\r\n"
//                   "Connection: close\r\n"
//                   "Authorization: Basic ",
//                   mount.c_str(),
//                   server_name.c_str());
//  int i = snprintf(buf, MAXDATASIZE - 40, /* leave some space for login */
//                   "GET /RTCM32_GGB HTTP/1.1\r\n"
//                   "User-Agent: NTRIP_YC\r\n"
//                   "Accept: */*\r\n"
//                   "Authorization: Basic ",
//                   server_name.c_str());
  int i = snprintf(buf, MAXDATASIZE - 40, /* leave some space for login */
                   "GET /%s HTTP/1.0\r\n"
                   "User-Agent: NTRIP YunChuan/2.0\r\n"
                   "Accept: */*\r\n"
                   "Connection: close\r\n"
                   "Authorization: Basic ",
                   mount.c_str());

  if (i > MAXDATASIZE - 40 || i < 0) {
    ROS_ERROR("[SOCKET]Requested data too long");
    return 0;
  } else {
    i += user_pwd_encode(buf + i, MAXDATASIZE - i - 4, user_name.c_str(), password.c_str());
    if (i > MAXDATASIZE - 4) {
      ROS_ERROR("[SOCKET]Username and/or password too long");
      return 0;
    } else {
      buf[i++] = '\r';
      buf[i++] = '\n';
      buf[i++] = '\r';
      buf[i++] = '\n';
      int j = snprintf(buf + i, MAXDATASIZE - i, "%s\r\n", nmea_gga.c_str());
      if (j >= 0 && j < MAXDATASIZE - i) {
        i += j;
      } else {
        ROS_ERROR("[SOCKET]NMEA string too long");
        return 0;
      }
    }
  }
  return i;
}

sensor_msgs::NavSatFix navsatfix_encode(Location &loc) {
  sensor_msgs::NavSatFix sat;
  sat.header.frame_id = "gnss";
  sat.header.stamp = ros::Time::now();

  sat.status.status = STATUS_NO_FIX;
  if (loc.fix == "0")
    sat.status.status = STATUS_NOT;
  if (loc.fix == "1")
    sat.status.status = STATUS_SINGLE;
  if (loc.fix == "2")
    sat.status.status = STATUS_DGPS;
  if (loc.fix == "4")
    sat.status.status = STATUS_FIX;
  if (loc.fix == "5")
    sat.status.status = STATUS_FLOAT;

  sat.status.service = SERVICE_GPS;

  std::string latitude_s = loc.lat;
  std::string longitude_s = loc.lon;
  double lat = atof(latitude_s.substr(0, 2).c_str()) + atof(latitude_s.substr(2, 2).c_str()) / 60.0 +
               atof(latitude_s.substr(5, 4).c_str()) / 600000.0;
  double lon = atof(longitude_s.substr(0, 3).c_str()) + atof(longitude_s.substr(3, 2).c_str()) / 60.0 +
               atof(longitude_s.substr(6, 4).c_str()) / 600000.0;
  sat.latitude = lat;
  sat.longitude = lon;

  sat.altitude = std::stod(loc.alt);

  static boost::array<double, 9> covariance = {1, 0, 0,
                                               0, 1, 0,
                                               0, 0, 1};
  std::copy(covariance.begin(), covariance.end(), sat.position_covariance.begin());
  sat.position_covariance[0] = std::stod(loc.hdop);
  sat.position_covariance[4] = std::stod(loc.hdop);
  sat.position_covariance[8] = 10;

  sat.position_covariance_type = COVARIANCE_TYPE_DIAGONAL_KNOWN;
//    ROS_INFO("[DGPS] lat: %f  | lon: %f | covariance[0]: %f covariance[4]: %f", sat.latitude, sat.longitude,
//             sat.position_covariance[0], sat.position_covariance[4]);
  return sat;
}

sensor_msgs::NavSatFix navsatfix_KSXT_encode(Location &loc) {
  sensor_msgs::NavSatFix sat;
  sat.header.frame_id = "gnss";
  sat.header.stamp = ros::Time::now();

  sat.status.status = STATUS_NO_FIX;
  if (loc.fix == "0")
    sat.status.status = STATUS_NOT;
  if (loc.fix == "1")
    sat.status.status = STATUS_SINGLE;
  if (loc.fix == "2")
    sat.status.status = STATUS_FLOAT;
  if (loc.fix == "3")
    sat.status.status = STATUS_FIX;

  sat.status.service = SERVICE_GPS;

//  std::string latitude_s = loc.lat;
//  std::string longitude_s = loc.lon;
//  double lat = atof(latitude_s.substr(0, 2).c_str()) + atof(latitude_s.substr(2, 2).c_str()) / 60.0 +
//               atof(latitude_s.substr(5, 4).c_str()) / 600000.0;
//  double lon = atof(longitude_s.substr(0, 3).c_str()) + atof(longitude_s.substr(3, 2).c_str()) / 60.0 +
//               atof(longitude_s.substr(6, 4).c_str()) / 600000.0;
//  sat.latitude = lat;
//  sat.longitude = lon;
  sat.latitude = std::stod(loc.lat);
  sat.longitude = std::stod(loc.lon);
  if (loc.yaw_fix == "3") {
    sat.altitude = std::stod(loc.yaw);
  } else {
    sat.altitude = 0;
  }

  static boost::array<double, 9> covariance = {1, 0, 0,
                                               0, 1, 0,
                                               0, 0, 1};
  std::copy(covariance.begin(), covariance.end(), sat.position_covariance.begin());
  sat.position_covariance[0] = 0.1;
  sat.position_covariance[4] = 0.1;
  sat.position_covariance[8] = 10;

  sat.position_covariance_type = COVARIANCE_TYPE_DIAGONAL_KNOWN;
//    ROS_INFO("[DGPS] lat: %f  | lon: %f | covariance[0]: %f covariance[4]: %f", sat.latitude, sat.longitude,
//             sat.position_covariance[0], sat.position_covariance[4]);
  return sat;
}

sensor_msgs::NavSatFix gptra_encode(double yaw) {
  sensor_msgs::NavSatFix sat;
  sat.header.frame_id = "gnss";
  sat.header.stamp = ros::Time::now();

  sat.altitude = yaw;
  return sat;
}

#endif //CODE_GNGGA_DECODE_H
