#include "controlcan.h"

#include <iostream>
#include <string>

int main()
{
	int nDT = 4;
	int nDId = 0;
	int nCId = 0;
	DWORD dwRel;

	dwRel = VCI_OpenDevice(nDT, nDId, 0);

	if (dwRel != 1) {
		std::cout << "open failed." << std::endl;
		return 1;
	}

	VCI_BOARD_INFO vbi;
	dwRel = VCI_ReadBoardInfo(nDT, nDId, &vbi);
	
	if (dwRel != 1) {
		std::cout << "error read" << std::endl;
		return 1;
	}

	printf("***********************************\n");
	printf("HW VERSION: %x\n", vbi.hw_Version);
	std::cout << (std::string)vbi.str_hw_Type << std::endl;

	VCI_INIT_CONFIG vic;
	vic.AccCode = 0x80000008;
	vic.AccMask = 0xFFFFFFFF;
	vic.Filter = 1;
	vic.Timing0 = 0x00;
	vic.Timing1 = 0x14;
	vic.Mode = 0;
	dwRel = VCI_InitCAN(nDT, nDId, nCId, &vic);

	if (dwRel != 1) {
		std::cout << "init error" << std::endl;
	       return 1;	
	}
	return 0;
}
