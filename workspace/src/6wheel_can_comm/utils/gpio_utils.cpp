#include "gpio_utils.h"

void initGpio(int n)
{
	FILE* fp = fopen("/sys/class/gpio/export", "w");
	if (NULL == fp) {
		perror("export open failed.");
	}
	else {
		fprintf(fp, "%d", n);
	}
	fclose(fp);
}

/**
 * @brief 
 *	设置 GPIO In 或 Out
 */
void setGpioDirection(int n, char* direction)
{
	char path[100] = {0};
	sprintf(path, "/sys/class/gpio/gpio%d/direction", n);

	FILE* fp = fopen(path, "w");

	if (NULL == fp) {
		perror("direction open failed.");
	}
	else {
		fprintf(fp, "%s", direction);
	}
	fclose(fp);
}

void setGpioValue(int n, int val) 
{
	char path[100] = {0};
	sprintf(path, "/sys/class/gpio/gpio%d/value", n);

	FILE* fp = fopen(path, "w");

	if (NULL == fp) {
		perror("value open failed.");
	}
	else {
		fprintf(fp, "%d", val);
	}
	fclose(fp);
}

int getGpioValue(int n)
{
	char path[64];
	char value_str[3];
	int fd;

	snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/direction", n);
	fd = open(path, O_RDONLY);
	if (fd < 0) {
		perror("Failed to open gpio value for reading!");
		return -1;
	}
	if (read(fd, value_str, 3) < 0) {
		perror("Failed to read value!");
		return -1;
	}
	close(fd);
	return (atoi(value_str));
}
