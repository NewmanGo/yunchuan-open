#ifndef GPIO_UTILS_H
#define GPIO_UTILS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

void initGpio(int n);
void setGpioDirection(int n, char* direction);
void setGpioValue(int n, int val);
int getGpioValue(int n);

#endif
