#ifndef CAN_COMM_H
#define CAN_COMM_H

#include "controlcan.h"
#include "gpio_utils.h"

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Int8MultiArray.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include "unistd.h"

#include <iostream>
#include <thread>

const int ROS_RATE_HZ = 30;
const double PI = M_PI;
const double SPEED_SCALE = 20 * 13 / 15; // 减速 链轮两端比例为15:13 减速机侧15
const double WHEEL_RADIUS_M = 0.104; // 0.086;
const double WHEEL_BASE_H = 0.5;
const double CMD_TIMEOUT_SEC = 0.4;

union Char2Int
{
  uint8_t chr[2];
  int int_value;
};

class CANComm
{
public:
  CANComm();
  ~CANComm();

  void initCAN();
  void execute();

private:
  /* Nodes */
  ros::NodeHandle m_n;
  ros::NodeHandle m_nh;

  /* Topics */
  ros::Subscriber m_cmd_sub;
  ros::Subscriber m_brk_sub;
  ros::Publisher m_odom_pub;
  /* Varibales */
  geometry_msgs::Twist m_cmd_vel;
  geometry_msgs::Twist m_base_twist;

  int m_speed[2] = {0};
  int m_ecd[3] = {0};
  int last_ecd[3] = {0};
  int m_num = 0;
  bool m_brk_flag = false;
  nav_msgs::Odometry m_base_odom;
  ros::Time m_timer_base;
  ros::Time m_timer_cmd;
  int m_motor_gpio = 56;

  /* Functions */
  void sendSpeed(int dev, int speed);
  void brake(int dev);
  void receiveMessage(int dev);
  void readEncoder();
  void enableMotors();
  void disableMotors();

  std::string fillZero(std::string input, int Dig)
  {
    std::string output;
    for (int i = 0; i < Dig - input.size(); ++i)
    {
      output += "0";
    }
    output += input;
    return output;
  }

  void hex2Num(int dev, std::vector<unsigned char> input);

  void publishOdom();

  double rpm2ms(int Input)
  {
    return (Input * 2 * PI * WHEEL_RADIUS_M) / (60 * SPEED_SCALE);
  }

  double ms2rpm(double Input)
  {
    double rpm = (Input * 60 * SPEED_SCALE) / (2 * PI * WHEEL_RADIUS_M);
    // *7525.72
    // std::cout << "rpm: " << rpm << std::endl;
    return rpm;
  }

  void CommandCallback(const geometry_msgs::Twist::ConstPtr &input)
  {
    m_cmd_vel = *input;
    float speed_right = (2 * m_cmd_vel.linear.x - WHEEL_BASE_H * m_cmd_vel.angular.z) / 2;
    float speed_left = (2 * m_cmd_vel.linear.x + WHEEL_BASE_H * m_cmd_vel.angular.z) / 2;
    m_speed[0] = static_cast<int>(ms2rpm(speed_right) * 8192. / 3000);
    m_speed[1] = static_cast<int>(-ms2rpm(speed_left) * 8192. / 3000);
    m_timer_cmd = ros::Time::now();
  }

  void BrakeCallback(const std_msgs::Int8MultiArray::ConstPtr &input)
  {
    m_brk_flag = !input->data[4];
  }
};

#endif
