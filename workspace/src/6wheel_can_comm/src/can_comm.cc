#include "can_comm.h"

CANComm::CANComm(): m_nh("~")
{
	m_cmd_sub =  m_n.subscribe("/cmd_vel", 1, &CANComm::CommandCallback, this);
    m_brk_sub = m_n.subscribe("/btn_ctl", 1, &CANComm::BrakeCallback, this);
	m_odom_pub = m_n.advertise<nav_msgs::Odometry>("/wheel_odom", 1);
	
	initCAN();

	m_base_odom.pose.pose.position.x = 0;
	m_base_odom.pose.pose.position.y = 0;
	m_base_odom.pose.pose.orientation = tf::createQuaternionMsgFromYaw(0);
	
	std::cout << "Init GPIO" << std::endl;
	initGpio(m_motor_gpio);
	setGpioDirection(m_motor_gpio, "out");
	setGpioValue(m_motor_gpio, 1); // 0
	std::cout << "GPIO Initiated" << std::endl;

}
	
CANComm::~CANComm()
{
	VCI_CloseDevice(VCI_USBCAN2, 0);
}

void CANComm::enableMotors()
{
	setGpioDirection(m_motor_gpio, "out");
	setGpioValue(m_motor_gpio, 0);

}

void CANComm::disableMotors()
{
	setGpioDirection(m_motor_gpio, "out");
	setGpioValue(m_motor_gpio, 1);

}

void CANComm::initCAN()
{
	VCI_BOARD_INFO m_info1[50];				// 存储设备序列号等信息
	m_num = VCI_FindUsbDevice2(m_info1);	// 查找当前设备

	if (VCI_OpenDevice(VCI_USBCAN2, 0, 0)==1) {		// 打开第一个 CAN 设备
		std::cout << "CAN Open Success!" << std::endl;
	}
	else {
		std::cout << "CAN Open Error!" << std::endl;
		return;
	}

	// 设置 CAN 参数
	VCI_INIT_CONFIG config;
	config.AccCode = 0;	
	config.AccMask = 0xFFFFFFFF;
	config.Filter = 1;				// 接受所有帧
	config.Timing0 = 0x00; 			// 波特率 1000 Kbps  0x00 0x1C
	config.Timing1 = 0x14;
	config.Mode = 0;				// 正常模式

	// 初始化并开启指定 CAN 通道
	if (VCI_InitCAN(VCI_USBCAN2, 0, 0, &config) != 1) {	// 通道 1
		std::cout << "Init CAN 1 Error!" << std::endl;
		VCI_CloseDevice(VCI_USBCAN2, 0);	// 关闭 CAN 设备
	}
	if (VCI_StartCAN(VCI_USBCAN2, 0, 0) != 1) {	// 开启通道 1
		std::cout << "Start CAN 1 Error!" << std::endl;
		VCI_CloseDevice(VCI_USBCAN2, 0);
	}

	if (VCI_InitCAN(VCI_USBCAN2, 0, 1, &config) != 1) {	// 通道 2
		std::cout << "Init CAN 1 Error!" << std::endl;
		VCI_CloseDevice(VCI_USBCAN2, 0);	// 关闭 CAN 设备
	}
	if (VCI_StartCAN(VCI_USBCAN2, 0, 1) != 1) {	// 开启通道 2
		std::cout << "Start CAN 1 Error!" << std::endl;
		VCI_CloseDevice(VCI_USBCAN2, 0);
	}


    DWORD dwRel;
    int nDeviceType = 4;
    VCI_BOARD_INFO vbi;
    dwRel = VCI_ReadBoardInfo(nDeviceType, 0, &vbi);
    if (dwRel != 1) {
        std::cout << "Read Error" << std::endl;
    } else {
        //printf("%x\n", vbi.str_Serial_Num);
    }
}
	
void CANComm::execute()
{
	ros::Rate loop_rate(ROS_RATE_HZ);
	while(ros::ok()) {
		loop_rate.sleep();
		ros::spinOnce();

	double command_delay = (ros::Time::now() - m_timer_cmd).toSec();

        	if (m_brk_flag || command_delay > 0.1) {
			std::cout << "command timeout" << std::endl;
			brake(1);
            		brake(2);
			receiveMessage(1);
        		receiveMessage(2);
			m_speed[0] = 0;
			m_speed[1] = 0;
			publishOdom();
            		continue;
        	}
		std::cout << "1: " << m_speed[0] << std::endl;
		std::cout << "2: " << m_speed[1] << std::endl;
    
    // 2024-3-4 六轮巡检与物流车底盘电机相反
		sendSpeed(2, m_speed[0]);
		sendSpeed(1, m_speed[1]);
		receiveMessage(1);
        	receiveMessage(2);
		publishOdom();
	}
}

void CANComm::sendSpeed(int dev, int speed)
{
	// 初始化发送帧结构
	VCI_CAN_OBJ send[1];
	send[0].ID = dev;	// 帧 ID
	send[0].SendType = 1;			// 单次发送
	send[0].RemoteFlag = 0;			// 数据帧
	send[0].ExternFlag = 0;			// 扩展帧
	send[0].DataLen = 8;			// 帧数据

    //std::cout << "send speed: " << speed << std::endl;
	// 电机协议部分，低位在前
	send[0].Data[0] = 0x00;         // 组号
	send[0].Data[1] = 0x1A;         // 写操作
	send[0].Data[2] = 0x06;         // 速度寄存器
	send[0].Data[3] = (speed&0xff00)>>8;    // 速度高位 
	send[0].Data[4] = speed&0x00ff;         // 速度低位
    send[0].Data[5] = 0x00;
	send[0].Data[6] = 0x00;
	send[0].Data[7] = 0x01;                 // 使能
    
    std::cout << "Dev: " << dev << std::endl;
    for (int i = 0; i < 8; ++i) {
      printf("%.2x", send[0].Data[i]);
    }
   std::cout << std::endl;
	// 发送
    int status = VCI_Transmit(VCI_USBCAN2, 0, dev-1, send, 1);
	if (status != 1) {
		std::cout << "Send Error! " << status << std::endl;
	}
}

void CANComm::readEncoder()
{
	VCI_CAN_OBJ send[1];
	send[0].ID = 0x0601;	// CAN 1
	send[0].SendType = 1;
	send[0].RemoteFlag = 0;
	send[0].ExternFlag = 0;
	send[0].DataLen = 8;
	
	// encoder 
	send[0].Data[0] = 0x40;
	send[0].Data[1] = 0x05;
	send[0].Data[2] = 0x21;
	send[0].Data[3] = 0x00;
	send[0].Data[4] = 0x00;
	send[0].Data[5] = 0x00;
	send[0].Data[6] = 0x00;
	send[0].Data[7] = 0x00;

	VCI_Transmit(VCI_USBCAN2, 0, 0, send, 1);
	//send[0].ID = 0x0602;
	//VCI_Transmit(VCI_USBCAN2, 0, 1, send, 1);
}

void CANComm::receiveMessage(int dev)
{
	VCI_CAN_OBJ rec1[3000];		//	通道 1 接受缓存
	int recLen1 = VCI_Receive(VCI_USBCAN2, 0, dev-1, rec1, 3000, 100);
	std::vector<unsigned char> output(2);
	if (recLen1 > 0) {
		for (int i = 0; i < recLen1; ++i) {
			// for (int j = 0; j < rec1[i].DataLen; ++j) {
			/*
				if (rec1[i].Data[j] == 0xE8){
					output[0] = rec1[i].Data[j+1];
					output[1] = rec1[i].Data[j+2];
				} 
				*/
				if (rec1[i].Data[5] == 0xE4) {
					output[0] = rec1[i].Data[6];
					output[1] = rec1[i].Data[7];
				}
				// printf("%02x", rec1[i].Data[j]);

			// }
			// std::cout << std::endl;
		}
		
		hex2Num(dev, output);


	} 
}

void CANComm::brake(int dev) 
{
	sendSpeed(dev, 0);
}

void CANComm::publishOdom()
{

	
	double x = m_base_odom.pose.pose.position.x;
	double y = m_base_odom.pose.pose.position.y;
	double th = tf::getYaw(m_base_odom.pose.pose.orientation);
	
	
	ros::Time current_time = ros::Time::now();
	
	//std::cout << m_ecd[1] / 8192.0 * 3000 << std::endl;
	
	double right_speed = (m_ecd[2] / 8192. * 3000) / 60 / SPEED_SCALE * 2 * PI * WHEEL_RADIUS_M ;
	double left_speed = -1 * (m_ecd[1] / 8192. * 3000) / 60 / SPEED_SCALE * 2 * PI * WHEEL_RADIUS_M;

	m_base_twist.linear.x = (left_speed + right_speed) / 2;
	m_base_twist.angular.z = (left_speed - right_speed) / WHEEL_BASE_H;

	double vx = m_base_twist.linear.x;
	double vth = m_base_twist.angular.z;
	double vy = 0;

	double dt = (current_time - m_timer_base).toSec();
	
	double delta_x = (vx * cos(th) - vy * sin(th)) * dt;
	double delta_y = (vx * sin(th) - vy * cos(th)) * dt;
	double delta_th = vth * dt;
	
	x += delta_x;
	y += delta_y;
	th += delta_th;

	geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

	odom_quat = tf::createQuaternionMsgFromYaw(th);
	m_base_odom.header.stamp = current_time;
	m_base_odom.header.frame_id = "odom";

	m_base_odom.pose.pose.position.x = x;
	m_base_odom.pose.pose.position.y = y;
	m_base_odom.pose.pose.orientation = odom_quat;
	
	m_base_odom.child_frame_id = "base_link";
	m_base_odom.twist.twist.linear.x = m_base_twist.linear.x;
	m_base_odom.twist.twist.linear.y = vy;
	m_base_odom.twist.twist.angular.z = m_base_twist.angular.z;
	m_odom_pub.publish(m_base_odom);

//	std::cout <<"x: " << m_base_odom.twist.twist.linear.x << std::endl;
//	std::cout <<"z: " << m_base_odom.twist.twist.angular.z << std::endl;

	m_ecd[1] = 0;
	m_ecd[2] = 0;
	m_base_odom.twist.twist.linear.x = 0;
	m_base_odom.twist.twist.linear.y = 0;
	m_base_odom.twist.twist.angular.z = 0;
	
	m_timer_base = current_time;
	
}



void CANComm::hex2Num(int dev, std::vector<unsigned char> input) {
	int part_1 = int(input[0]);
	int part_2 = int(input[1]);
	//std::cout << part_2 << std::endl;
	std::stringstream ss_1;
	std::stringstream ss_2;

	ss_1 << std::hex << part_1;
	ss_2 << std::hex << part_2;

	std::string output_1_str = fillZero(ss_1.str(), 2);
	std::string output_2_str = fillZero(ss_2.str(), 2);
	
	std::string output = output_1_str + output_2_str;
	//std::cout << "out: " << output << std::endl;
	// todo: bu ma dao yuan ma 
	int output_int = std::stoi(output, nullptr, 16);
	m_ecd[dev] = input[0]>=0xAF?(output_int - 65535):output_int;
}
