#include "can_comm.h"

int main(int argc, char** argv)
{
	ros::init(argc, argv, "can_comm_node");
	CANComm MyControl;
	MyControl.execute();

	return 0;
}
