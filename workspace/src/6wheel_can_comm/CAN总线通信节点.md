# CAN 总线通信
## 一、协议
1. 驱动器协议文档：`AQMD6010BLS-Ex_UM_V0.90.pdf`
2. 周立功 CAN 分析仪文档： `./CAN分析仪资料/说明文档目录`
## 二、话题
|话题|类型|描述|
|-|-|-|
|/cmd_vel|geometry_msgs::Twist|接受速度话题|
|/btn_ctl|std_msgs::Int8Multiarray|接受按键话题|
|/wheel_odom|nav_msgs::Odometry|发布里程计信息话题|
|/battery_info|geometry_msgs::Point32|发布电池电量信息|
## 三、API

|函数名|返回值|描述|
|-|-|-|
|initCAN|void|初始化CAN分析仪，如果没有权限则会失败|
|publishOdom|void|发布里程计信息|
|sendSpeed|void|发送速度指令到电机|
|brake|void|刹车|
|receiveMessage|void|接受来自驱动器的帧|
|readEncoder|void|读取处理编码器信息|
|publishBatteryVoltage|void|发布电池当前电量|

## 四、问题记录
1. AIO-3399C 补码-源码转换存在问题，负浮点数会变成零，且无法用 C++ 标准库 stoi 或 union 等常规方法转化。
2. 周立功 CAN 分析仪提供的动态库在 PC 和 3399 需要使用不同的动态库文件
3. CAN 节点需要工作在 root 权限，或者在插入设备后手动对 /dev/bus/usb/ 目录下对应的设备加权限。由于设备号冲突，因此无法进行软连接