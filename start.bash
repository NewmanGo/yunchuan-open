#!/bin/bash

bash_core='/opt/ros/melodic/setup.bash'
core='roscore'

# Environment
base_ws='/'


workspace='/home/firefly/workspace'
bash_end='/devel/setup.bash'
launch_end='.launch'

# Launch File
# Base Launch
launch_livox='livox_ros_driver2 rviz_MID360'
launch_kruisee='kruisee_lidar kruisee_lidar'


# Functions
set_permission() {
	sleep 1
   	sudo chmod 777 /dev/ttyUSB*
    	echo 'set permission.'
}

start_core() {
	sleep 3
	gnome-terminal -e "bash -c \" source ${workspace}${auto_ws}${bash_end};\
	roscore; \
	exec bash \""
	sleep 8
}

start_gps() {
	sleep 2
	gnome-terminal -e "bash -c \" echo 'start dgps...'; \
    export ROS_NODE_LOG_DIR=gps
		source ${workspace}${base_ws}${bash_end};	\
		roslaunch nmea_comms yc_r9310_gnss.launch; \
        exec bash\""
}

start_can(){
	sleep 1
	gnome-terminal -e "bash -c \" echo 'start can_comm info...'; \
		source ${workspace}${base_ws}${bash_end}; \
    	sudo chmod 777 /sys/class/gpio/*; \
		sudo echo 56 > /sys/class/gpio/export; \
		sudo chmod 777 /sys/class/gpio/gpio56/*; \
    sudo chmod 777 /dev/bus/usb/003/00*; \
		sudo chmod 777 /dev/bus/usb/004/00*; \
		sudo chmod 777 /dev/bus/usb/005/00*;	\
		sudo chmod 777 /dev/bus/usb/006/00*;	\
		rosrun can_comm can_comm_node; \
		exec bash \""
}

start_kruisee() {
  sleep 2
	gnome-terminal -e "bash -c \" echo 'start kruisee...'; \
    export ROS_NODE_LOG_DIR=kruisee
		source ${workspace}${base_ws}${bash_end};	\
		roslaunch ${launch_kruisee}${launch_end}; \
        	exec bash\""
}

start_livox_ros_driver2() {
        sleep 2
        gnome-terminal -e "bash -c \" echo 'start livox_ros_driver2...'; \
    export ROS_NODE_LOG_DIR=livox_ros_driver2
                source ${workspace}${base_ws}${bash_end};   \
                roslaunch ${launch_livox}${launch_end}; \
                exec bash\""
}



# Start
start_core

set_permission
start_kruisee
start_can
start_gps
start_livox_ros_driver2


exit 0
